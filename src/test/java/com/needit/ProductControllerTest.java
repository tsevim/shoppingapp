package com.needit;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest

public class ProductControllerTest extends AbstractTest {
	String prefixUri = "/api";

	@Override
	@Before
	public void setUp() throws IOException {
		super.setUp();
	}

	public boolean isQueryCorrect(JSONArray resp, String queryKey, String queryValue) throws JSONException {
		for(int i = 0; i<resp.length();i++){
			if(!(resp.getJSONObject(i).getString(queryKey).equals(queryValue)))
				return false;
		}
		return true;
	}

	@Test
	public void searchProduct() throws Exception {
		String uri = prefixUri + "/products?category=instrument";
		JSONArray resp = getAsJSONArray(uri);
		assertEquals(isQueryCorrect(resp, "category", "instrument"), true);
		assertEquals(isQueryCorrect(resp, "category", "technology"), false);
		assertEquals(isQueryCorrect(resp, "category", "clothing"), false);

		
		uri = prefixUri + "/products?category=technology";
		resp = getAsJSONArray(uri);
		assertEquals(isQueryCorrect(resp, "category", "technology"), true);
		assertEquals(isQueryCorrect(resp, "category", "instrument"), false);
		assertEquals(isQueryCorrect(resp, "category", "clothing"), false);

		uri = prefixUri + "/products?category=clothing";
		resp = getAsJSONArray(uri);
		assertEquals(isQueryCorrect(resp, "category", "clothing"), true);
		assertEquals(isQueryCorrect(resp, "category", "instrument"), false);
		assertEquals(isQueryCorrect(resp, "category", "technology"), false);

	}



	@Test
	public void viewProducts() throws Exception {
		String uri = prefixUri + "/products";
		JSONArray resp = getAsJSONArray(uri);
		assertEquals(resp.length(), 234); 
	}

}
