package com.needit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.needit.config.Session;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LoginControllerTest extends AbstractTest {
	String prefixUri = "/api";

	@Override
	@Before
	public void setUp() throws IOException {
		super.setUp();
	}

	public int postTest(Map<String, String> user, String uri) throws Exception {
		String inputJson = super.mapToJson(user);
		MvcResult mvcResult = mvc.perform(
				MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
				.andDo(MockMvcResultHandlers.print()).andReturn();
		int status = mvcResult.getResponse().getStatus();
		// if(authorizedLogin)
		// sessionId = (String) mvcResult.getResponse().getHeaderValue("session-id");
		return status;
	}

	public Map<String, String> createUser(String email, String password, String name, String surname, String type) {
		Map<String, String> user = new HashMap<>();

		user.put("email", email);
		user.put("password", password);
		user.put("name", name);
		user.put("surname", surname);
		user.put("type", type);

		return user;
	}

	@Test
	public void addUser() throws Exception {
		String reqUri = prefixUri + "/register";
		// New user
		Map<String, String> newUser = createUser("test" + System.currentTimeMillis() + "@gmail.com", "test1test1",
				"testname", "testsurname", "seller");
		int status = postTest(newUser, reqUri);
		assertEquals(200, status); // user registered

		// Existing user (customer only)
		Map<String, String> existingUser = createUser("cselcukguvel@gmail.com", "12345678c", "Selcuk", "Guvel",
				"customer");
		status = postTest(existingUser, reqUri);
		assertEquals(400, status); // existing user
	}

	@Test

	public void loginHandler() throws Exception {
		String uri = prefixUri + "/login";

		// Authorized user
		Map<String, String> authorizedUser = createUser("ctahasevim@gmail.com", "12345678c", "Taha", "Sevim",
				"customer");
		int status = postTest(authorizedUser, uri);
		assertEquals(200, status); // authorized user

		// Unauthorized user
		Map<String, String> unauthorizedUser = createUser("cselcukguvel@gmail.com", "12345678cwrong", "Selcuk", "Guvel",
				"customer");
		status = postTest(unauthorizedUser, uri);
		assertEquals(401, status); // unauthorized user

		// Banned user
		Map<String, String> bannedUser = createUser("cselcukguvel@gmail.com", "12345678c", "Selcuk", "Guvel",
				"customer");
		status = postTest(bannedUser, uri);
		assertEquals(401, status); // banned user

	}

	@Test
	public void logoutHandler() throws Exception { // tests getCard function in the LoginController.java

		// First login with authorized user
		String uri = prefixUri + "/login";


		
		// Authorized user
		Map<String, String> authorizedUser = createUser("ctahasevim@gmail.com", "12345678c", "Taha", "Sevim",
				"customer");
		String inputJson = super.mapToJson(authorizedUser);
		MvcResult mvcResult = mvc.perform(
				MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
				.andDo(MockMvcResultHandlers.print()).andReturn();
		int status = mvcResult.getResponse().getStatus();
		String respString = mvcResult.getResponse().getContentAsString();
		assertEquals(200, status); // authorized user
		String reqid;
		String respUserId = respString.split(",")[0].split(":")[1].replace("\"", ""); // Fix later by deserializing
		String respSessionId = (String) mvcResult.getResponse().getHeaderValue("session-id");

		String userId = "d20a350a37d64c12"; // user id of Taha

		// fix later
		// If the user id is true and the session id is not empty then log out is
		// successful
		assertEquals(userId, respUserId);
		assertNotEquals(respSessionId, "");

		// String uri = prefixUri + "/logout";
		// Map<String, String> logoutInfo = new HashMap<>();
		// String userId = "d20a350a37d64c12"; // user id of Taha
		// logoutInfo.put("id", userId);
		// logoutInfo.put("type", "customer");
		// String inputJson = super.mapToJson(logoutInfo);
		// HashMap<String, String> customerSessions = Session.getCustomerSessions();
		// String sessionId = customerSessions.get(userId);
		// // MvcResult mvcResult =
		// // mvc.perform(MockMvcRequestBuilders.post(uri).header("session-id",
		// sessionId)
		// //
		// .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
		// // int status = mvcResult.getResponse().getStatus();
		// assertEquals(200, customerSessions); // user is logged out successfully
	}
}
