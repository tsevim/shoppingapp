package com.needit;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

//import org.springframework.http.MediaType;
//import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest

public class CardControllerTest extends AbstractTest {
    String prefixUri = "/api/customer";
    
	@Override
	@Before
	public void setUp() throws IOException {
		super.setUp();
	}

	@Test
    public void viewCard() throws Exception {
        String uri = prefixUri + "/d20a350a37d64c12/card"; // ctahasevim card
        int currentItemNum = 76; //fix later
        JSONArray resp = getAsJSONArray(uri);
        assertEquals(getStatus(uri), 200);
        assertEquals(resp.length(), currentItemNum);
    }

}
