package com.needit.controller;

import java.util.HashMap;
import java.util.List;

import com.needit.model.Product;
import com.google.api.client.util.Data;
import com.needit.config.Session;
import com.needit.model.Admin;
import com.needit.model.Customer;
import com.needit.model.User;
import com.needit.utility.DatabaseWrapper;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/api")
public class AdminController {

    @RequestMapping(method = RequestMethod.PUT, value = "/admin/{aid}")
    public void updateCustomer(@PathVariable String aid, @RequestHeader("session-id") String key, 
    @RequestBody HashMap<String, String> body) throws InterruptedException {
        if (!(Session.isValid("admin", aid, key)))
            return;
        Admin admin = (Admin) DatabaseWrapper.getUser("admins", aid);
        String name = body.get("name");
        System.out.println(name);
        String surname = body.get("surname");
        System.out.println(surname);

        String password = body.get("password");
        admin.setName(name);
        admin.setSurname(surname);
        admin.setPassword(password);
        DatabaseWrapper.addAdmin(admin);
        System.out.println("Admin updated");
    }
}