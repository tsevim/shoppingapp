package com.needit.controller;

import java.util.HashMap;
import java.util.List;

import com.needit.model.Product;
import com.needit.model.Seller;
import com.google.api.client.util.Data;
import com.needit.config.Session;
import com.needit.model.Card;
import com.needit.model.Customer;
//import com.needit.model.User;
import com.needit.utility.DatabaseWrapper;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/api")
public class CustomerController {

    @RequestMapping(method = RequestMethod.GET, value = "/customers")
    public ResponseEntity<List<Customer>> CustomerList(@RequestHeader("session-id") String key,
            @RequestHeader("aid") String aid) throws InterruptedException {
        if (!(Session.isValid("admin", aid, key)))
            return null;
        List<Customer> customers = (List<Customer>) (List<?>) DatabaseWrapper.getAllUsers("customers");
        return new ResponseEntity<List<Customer>>(customers, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/customer/{cid}")
    public ResponseEntity<Customer> getCustomer(@PathVariable String cid, @RequestHeader("session-id") String key,
            @RequestHeader("aid") String aid) throws InterruptedException {
        if (!(Session.isValid("admin", aid, key)))
            return null;
        Customer customer = (Customer) DatabaseWrapper.getUser("customers", cid);
        System.out.println(customer + ":" + cid);
        return new ResponseEntity<Customer>(customer, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/customer")
    public void createCustomer(@RequestBody Customer customer, @RequestHeader("session-id") String key,
            @RequestHeader("aid") String aid) {
        if (!(Session.isValid("admin", aid, key)))
            return;
        DatabaseWrapper.addCustomer(customer);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/customer/{cid}")
    public void updateCustomer(@PathVariable String cid, @RequestHeader("session-id") String key,
            @RequestHeader("aid") String aid, @RequestBody HashMap<String, String> body) throws InterruptedException {
        if (!(Session.isValid("admin", aid, key)) && !(Session.isValid("customer", cid, key)))
            return;
        Customer customer = (Customer) DatabaseWrapper.getUser("customers", cid);
        String name = body.get("name");
        System.out.println(name);
        String surname = body.get("surname");
        System.out.println(surname);

        String password = body.get("password");
        customer.setName(name);
        customer.setSurname(surname);
        customer.setPassword(password);
        DatabaseWrapper.addCustomer(customer);
        System.out.println("Customer updated");
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/customer/{cid}")
    public void deleteProduct(@PathVariable String cid, @RequestHeader("session-id") String key,
            @RequestHeader("aid") String aid) {
        if (!(Session.isValid("admin", aid, key)))
            return;
        DatabaseWrapper.deleteUser("customers", cid);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/customer/checkout/{cid}")
    public void checkout(@PathVariable String cid, @RequestHeader("session-id") String key) {
        if (!(Session.isValid("customer", cid, key)))
            return;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/customer/buy/{cid}")
    public ResponseEntity<?>  buy(@PathVariable String cid, @RequestHeader("session-id") String key) throws InterruptedException {
        if (!(Session.isValid("customer", cid, key)))
            return null;
        Customer custom = (Customer) DatabaseWrapper.getUser("customers", cid);
        List<Seller> sellers = (List<Seller>) (List<?>) DatabaseWrapper.getAllUsers("sellers");
        Card customCard = custom.getShoppingCard();
        Card newCard = new Card();
        for (String pid : customCard.getProductIDs().keySet()){
            int count = customCard.getProductIDs().get(pid);
            boolean isValid = decreaseStock(sellers, pid, count);
            if (!isValid){
                for(int i = 0; i<count;i++)
                    newCard.addToProductIDs(pid);
                continue;
            }
            DatabaseWrapper.deleteProduct("instrument", pid);
            DatabaseWrapper.deleteProduct("clothing", pid);
            DatabaseWrapper.deleteProduct("technology", pid);
        }
        DatabaseWrapper.updateCustomerCard(cid, newCard);

        return new ResponseEntity<>(HttpStatus.OK);

    }

    private boolean decreaseStock(List<Seller> sellers, String pid, int count){
        for(Seller s : sellers){
            HashMap<String, Integer> productIDs = s.getProductIDs();
            if (productIDs == null)
                productIDs = new HashMap<String, Integer>();
            if (productIDs.containsKey(pid) && count <= productIDs.get(pid)){
                int remain = productIDs.get(pid) - count;
                if (remain == 0)
                    productIDs.remove(pid);
                else
                    productIDs.put(pid, remain);
                DatabaseWrapper.updateSellerProducts(s.getId(), productIDs);
                return true;
            } 
        }
        return false;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/customer/ban/{cid}")
    public void banUser(@PathVariable String cid, @RequestHeader("session-id") String key,
            @RequestHeader("aid") String aid) throws InterruptedException {
        if (!(Session.isValid("admin", aid, key)))
            return;
        Customer custom = (Customer) DatabaseWrapper.getUser("customers", cid);
        custom.setStatus("banned");
        DatabaseWrapper.addCustomer(custom);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/customer/unban/{cid}")
    public void unbanUser(@PathVariable String cid, @RequestHeader("session-id") String key,
            @RequestHeader("aid") String aid) throws InterruptedException {
        if (!(Session.isValid("admin", aid, key)))
            return;
        Customer custom = (Customer) DatabaseWrapper.getUser("customers", cid);
        custom.setStatus("normal");
        DatabaseWrapper.addCustomer(custom);
    }


}