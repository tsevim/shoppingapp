package com.needit.controller;

import java.util.HashMap;
import java.util.List;

import com.needit.config.Session;
import com.needit.model.Admin;
import com.needit.model.Customer;
import com.needit.model.Seller;
//import com.needit.model.Seller;
import com.needit.model.User;
import com.needit.utility.DatabaseWrapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/api")
public class LoginController {

    private final Logger log = LoggerFactory.getLogger(LoginController.class);

    @RequestMapping(method = RequestMethod.POST, value = "/register")
    public ResponseEntity<User> addUser(@RequestBody HashMap<String, String> body) throws InterruptedException {
        List<User> users = DatabaseWrapper.getAllUsers("customers");
        String email = body.get("email");
        String password = body.get("password");
        String name = body.get("name");
        String surname = body.get("surname");
        String type = body.get("type");
        for (User u : users)
            if (u.getEmail().equals(email)) {
                log.info("Attempted to register an existing user");
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        users = DatabaseWrapper.getAllUsers("sellers");
        for (User u : users)
            if (u.getEmail().equals(email)) {
                log.info("Attempted to register an existing user");
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        users = DatabaseWrapper.getAllUsers("admins");
        for (User u : users)
            if (u.getEmail().equals(email)) {
                log.info("Attempted to register an existing user");
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }

        if (type.equals("customer")) {
            DatabaseWrapper.addCustomer(new Customer(email, password, name, surname));
        }

        else if (type.equals("seller")) {
            DatabaseWrapper.addSeller(new Seller(email, password, name, surname, "x"));
        }

        log.info("user registered");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/login")
    public ResponseEntity<?> loginHandler(@RequestBody HashMap<String, String> body) throws InterruptedException {
        log.info("testing...");
        String email = body.get("email");
        String password = body.get("password");
        List<User> users = DatabaseWrapper.getAllUsers("customers");
        users.addAll(DatabaseWrapper.getAllUsers("sellers"));
        users.addAll(DatabaseWrapper.getAllUsers("admins"));
        log.info("checking user credentials....");
        HttpHeaders headers = new HttpHeaders();
        for (User u : users)
            if (u.getEmail().equals(email) && u.getPassword().equals(password)) {
                String key = Session.generateSessionKey();
                headers.add("session-id", key);
                if (u instanceof Customer)
                    Session.addSessionKey("customer", u.getId(), key);
                else if (u instanceof Admin)
                    Session.addSessionKey("admin", u.getId(), key);
                else if (u instanceof Seller)
                    Session.addSessionKey("seller", u.getId(), key);
                if (u.getStatus().equals("banned")) {
                    log.info("banned user");
                    return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
                }
                log.info("success");
                return ResponseEntity.ok().headers(headers).body(u);
            }
        log.info("fail");
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/logout")
    public ResponseEntity<List<HashMap<String, String>>> getCard(@RequestHeader("session-id") String key,
            @RequestBody HashMap<String, String> body) throws InterruptedException {
        String id = body.get("id");
        String type = body.get("type");
        if (!(Session.isValid(type, id, key)))
            return null;
        System.out.println("Logged out");
        Session.removeSessionKey(type, id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}