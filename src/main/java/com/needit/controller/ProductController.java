package com.needit.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.needit.model.Clothing;
import com.needit.model.Instrument;
import com.needit.model.Product;
import com.needit.model.Seller;
import com.needit.model.Technology;
import com.needit.utility.DatabaseWrapper;

import org.hibernate.boot.model.relational.Database;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/api")
public class ProductController {

    private List<Product> filterProducts(List<Product> productList, String key, String value) {
        List<Product> filtered = new ArrayList<Product>();
        for (Product p : productList) {
            if (key.equals("name")) {
                if (p.getName().equals(value))
                    filtered.add(p);

            } else if (key.equals("brand")) {
                if (p.getBrand().equals(value))
                    filtered.add(p);
            }
        }
        return filtered;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/products")
    public ResponseEntity<List<HashMap<String, String>>> getFiltered(@RequestParam Map<String, String> allParams)
            throws InterruptedException {
        List<HashMap<String, String>> response = new ArrayList<HashMap<String, String>>();
        if (allParams.isEmpty()) {
            List<Instrument> insts = (List<Instrument>) (List<? extends Product>) DatabaseWrapper
                    .getProducts("instrument");
            List<Technology> techs = (List<Technology>) (List<? extends Product>) DatabaseWrapper
                    .getProducts("technology");
            List<Clothing> cloths = (List<Clothing>) (List<? extends Product>) DatabaseWrapper.getProducts("clothing");
            techConcat(response, techs);
            clothConcat(response, cloths);
            instConcat(response, insts);
            return new ResponseEntity<List<HashMap<String, String>>>(response, HttpStatus.OK);
        }
        String name = null;
        String category = null;
        String brand = null;
        if (allParams.containsKey("category"))
            category = allParams.get("category");
        if (allParams.containsKey("name"))
            name = allParams.get("name");
        if (allParams.containsKey("brand"))
            brand = allParams.get("brand");
        if (category == null) {
            List<Product> insts = DatabaseWrapper.getProducts("instrument");
            List<Product> techs = DatabaseWrapper.getProducts("technology");
            List<Product> cloths = DatabaseWrapper.getProducts("clothing");

            if (name != null) {
                techs = filterProducts(techs, "name", name);
                insts = filterProducts(insts, "name", name);
                cloths = filterProducts(cloths, "name", name);
            }
            if (brand != null) {
                techs = filterProducts(techs, "brand", brand);
                insts = filterProducts(insts, "brand", brand);
                cloths = filterProducts(cloths, "brand", brand);
            }
            techConcat(response, (List<Technology>) (List<? extends Product>) techs);
            clothConcat(response, (List<Clothing>) (List<? extends Product>) cloths);
            instConcat(response, (List<Instrument>) (List<? extends Product>) insts);

        }

        else if (category.equals("technology")) {
            List<Product> techs = DatabaseWrapper.getProducts("technology");
            if (name != null)
                techs = filterProducts(techs, "name", name);
            if (brand != null)
                techs = filterProducts(techs, "brand", brand);
            techConcat(response, (List<Technology>) (List<? extends Product>) techs);
        }

        else if (category.equals("instrument")) {
            List<Product> insts = DatabaseWrapper.getProducts("instrument");
            if (name != null)
                insts = filterProducts(insts, "name", name);
            if (brand != null)
                insts = filterProducts(insts, "brand", brand);
            instConcat(response, (List<Instrument>) (List<? extends Product>) insts);

        }

        else if (category.equals("clothing")) {
            List<Product> cloths = DatabaseWrapper.getProducts("clothing");
            if (name != null)
                cloths = filterProducts(cloths, "name", name);
            if (brand != null)
                cloths = filterProducts(cloths, "brand", brand);
            clothConcat(response, (List<Clothing>) (List<? extends Product>) cloths);
        }
        return new ResponseEntity<List<HashMap<String, String>>>(response, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/product")
    public ResponseEntity<Product> getProduct(@RequestBody HashMap<String, String> body) throws InterruptedException {
        String category = body.get("category");
        String id = body.get("id");
        Product product = DatabaseWrapper.getProduct(category, id);
        return new ResponseEntity<Product>(product, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/product")
    public void createProduct(@RequestBody HashMap<String, String> body) throws InterruptedException {
        String category = body.get("category");
        Product p = mapToProduct(body);
        if (body.containsKey("id"))
            p.setId(body.get("id"));
        if (category.equals("technology"))
            DatabaseWrapper.addTechnology((Technology) p);

        else if (category.equals("instrument"))
            DatabaseWrapper.addInstrument((Instrument) p);

        else if (category.equals("clothing"))
            DatabaseWrapper.addClothing((Clothing) p);
        if (body.containsKey("sellerId")) {
            Seller s = (Seller) DatabaseWrapper.getUser("sellers", body.get("sellerId"));
            if (s.getProductIDs() == null)
                s.setProductIDs(new HashMap<String, Integer>());
            s.addToProductIds(p.getId(), 1);
            DatabaseWrapper.updateSellerProducts(s.getId(), s.getProductIDs());
        }
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/product")
    public void deleteProduct(@RequestBody HashMap<String, String> body) throws InterruptedException {
        String category = body.get("category");
        String id = body.get("id");
        String sid = body.get("sellerId");
        Seller s = (Seller) DatabaseWrapper.getUser("sellers", sid);
        HashMap<String, Integer> productIDs = s.getProductIDs();
        productIDs.remove(id);
        DatabaseWrapper.updateSellerProducts(sid, productIDs);
        DatabaseWrapper.deleteProduct(category, id);
    }

    private void techConcat(List<HashMap<String, String>> response, List<Technology> products) {
        for (Technology t : products) {
            HashMap<String, String> mp = productToMap(t);
            mp.put("device", t.getDevice());
            mp.put("additionalInfo", t.getAdditionalInfo());
            mp.put("category", "technology");
            response.add(mp);
        }
    }

    private void instConcat(List<HashMap<String, String>> response, List<Instrument> products) {
        for (Instrument i : products) {
            HashMap<String, String> mp = productToMap(i);
            mp.put("type", i.getType());
            mp.put("isElectro", String.valueOf(i.getIsElectro()));
            mp.put("woodType", i.getWoodType());
            mp.put("category", "instrument");
            response.add(mp);
        }
    }

    private void clothConcat(List<HashMap<String, String>> response, List<Clothing> products) {
        for (Clothing c : products) {
            HashMap<String, String> mp = productToMap(c);
            mp.put("size", c.getSize());
            mp.put("gender", c.getGender());
            mp.put("category", "clothing");
            response.add(mp);
        }
    }

    public static HashMap<String, String> productToMap(Product p) {
        HashMap<String, String> mp = new HashMap<String, String>();
        mp.put("id", p.getId());
        mp.put("name", p.getName());
        mp.put("brand", p.getBrand());
        mp.put("price", String.valueOf(p.getPrice()));
        mp.put("imageUrl", p.getImageUrl());
        mp.put("information", p.getInformation());
        mp.put("color", p.getColor());
        mp.put("sellerId", p.getSellerId());
        mp.put("stock", String.valueOf(p.getStock()));
        return mp;
    }

    private Product mapToProduct(HashMap<String, String> map) {
        Product p = null;
        String sid = map.get("sellerId");
        String color = map.get("color");
        String price = map.get("price");
        String imageUrl = map.get("imageUrl");
        String information = map.get("information");
        String name = map.get("name");
        String id = map.get("id");
        String category = map.get("category");
        String brand = map.get("brand");
        String stock = map.get("stock");
        if (category.equals("technology")) {
            String additional = map.get("additionalInfo");
            String device = map.get("device");
            Technology tech = new Technology(name, brand, Double.parseDouble(price), imageUrl, information, color, sid,
                    Integer.parseInt(stock), device, additional);
            // tech.setId(id);
            return tech;
        } else if (category.equals("instrument")) {
            boolean isElectro = Boolean.parseBoolean(map.get("isElectro"));
            String type = map.get("type");
            String woodType = map.get("woodType");
            Instrument inst = new Instrument(name, brand, Double.parseDouble(price), imageUrl, information, color, sid,
                    Integer.parseInt(stock), type, isElectro, woodType);
            // inst.setId(id);
            return inst;
        }

        else if (category.equals("clothing")) {
            String gender = map.get("gender");
            String size = map.get("size");
            Clothing cloth = new Clothing(name, brand, Double.parseDouble(price), imageUrl, information, color, sid,
                    Integer.parseInt(stock), size, gender);
            // cloth.setId(id);
            return cloth;
        }
        return null;
    }
}