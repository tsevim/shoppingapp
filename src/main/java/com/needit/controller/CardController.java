package com.needit.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.needit.config.Session;
import com.needit.model.Card;
import com.needit.model.Customer;
import com.needit.model.Product;
import com.needit.utility.DatabaseWrapper;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/api/customer")
public class CardController {

    
    @RequestMapping(method = RequestMethod.GET, value="/{id}/card")
    public ResponseEntity<List<HashMap<String, String>>> getCard(@PathVariable String id, @RequestHeader("session-id") String key) throws InterruptedException {
        if (! (Session.isValid("customer", id, key)))
           return null;
        Card card = ((Customer)DatabaseWrapper.getUser("customers", id)).getShoppingCard();
        List<HashMap<String, String>> response = new ArrayList<HashMap<String, String>>();
        for(String pid : card.getProductIDs().keySet()){
            HashMap<String, String> pmap = null;
            Product pt = DatabaseWrapper.getProduct("technology", pid);
            Product pc = DatabaseWrapper.getProduct("clothing",  pid);
            Product pi = DatabaseWrapper.getProduct("instrument", pid);
            if (pt != null){
                pmap = ProductController.productToMap(pt);
                pmap.put("category", "technology");
            }
            else if(pc != null){
                pmap = ProductController.productToMap(pc);
                pmap.put("category", "clothing");
            }
            else if(pi != null){
                pmap = ProductController.productToMap(pi);
                pmap.put("category", "instrument");
            } else {
                return new ResponseEntity<List<HashMap<String, String>>>(response, HttpStatus.OK);
            }
            pmap.put("qty", String.valueOf(card.getProductIDs().get(pid)));
            response.add(pmap);
        }
        return new ResponseEntity<List<HashMap<String, String>>>(response, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value="/{id}/card")
    public ResponseEntity<String> addToCard(@PathVariable String id, @RequestHeader("session-id") String key,
    @RequestBody List<HashMap<String, String>> body) throws InterruptedException {
        if (! (Session.isValid("customer", id, key)))
            return null;
        Card card = new Card();
        HashMap<String, Integer> count = new HashMap<String, Integer>();
        int total  = 0;
        for(HashMap<String, String> mp: body){
            count.put(mp.get("id"), (int)Double.parseDouble(mp.get("qty")));
            total  += Double.parseDouble(mp.get("price"));
        }
        card.setProductIDs(count);
        card.setPrice(total);
        DatabaseWrapper.updateCustomerCard(id, card);
        return new ResponseEntity<String>("added", HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT, value="/{id}/card")
    public ResponseEntity<String> updateCard(@PathVariable String id, @RequestHeader("session-id") String key,
    @RequestBody HashMap<String, String> body) throws InterruptedException {
        System.out.println(body);
        if (! (Session.isValid("customer", id, key)))
            return null;
        Card card = ((Customer)DatabaseWrapper.getUser("customers", id)).getShoppingCard();
        HashMap<String, Integer> mp = card.getProductIDs();
        String pid = body.get("id");
        String category = body.get("category");
        System.out.println(body);
        int count = Integer.parseInt(body.get("qty"));
        card.updateQty(category, pid, count);
        DatabaseWrapper.updateCustomerCard(id, card);
        return new ResponseEntity<String>("updated", HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, value="/{id}/card")
    public ResponseEntity<String> deleteProductFromCard(@PathVariable String id, @RequestHeader("session-id") String key,
    @RequestBody HashMap<String, String> body) throws InterruptedException {
        System.out.println(body);
        if (! (Session.isValid("customer", id, key)))
            return null;
        Card card = ((Customer)DatabaseWrapper.getUser("customers", id)).getShoppingCard();
        String pid = body.get("id");
        String category = body.get("category");

        card.removeProduct(category, pid);
        DatabaseWrapper.updateCustomerCard(id, card);
        return new ResponseEntity<String>("deleted", HttpStatus.OK);
    }

}