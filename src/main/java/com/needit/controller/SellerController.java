package com.needit.controller;

import java.util.List;

import com.needit.config.Session;
import com.needit.model.Product;
import com.needit.model.Seller;
import com.needit.model.User;
import com.needit.utility.DatabaseWrapper;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/api")
public class SellerController {

    @RequestMapping(method = RequestMethod.GET, value = "/sellers")
    public ResponseEntity<List<Seller>> SellerList(@RequestHeader("session-id") String key,
            @RequestHeader("aid") String aid) throws InterruptedException {
        if (!(Session.isValid("admin", aid, key)))
            return null;
        List<Seller> sellers = (List<Seller>) (List<?>) DatabaseWrapper.getAllUsers("sellers");

        return new ResponseEntity<List<Seller>>(sellers, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/seller/{sid}")
    public ResponseEntity<Seller> getSeller(@PathVariable String sid, @RequestHeader("session-id") String key,
            @RequestHeader("aid") String aid) throws InterruptedException {
        if (!(Session.isValid("admin", aid, key)))
            return null;
        Seller Seller = (Seller) DatabaseWrapper.getUser("sellers", sid);
        System.out.println(Seller + ":" + sid);
        return new ResponseEntity<Seller>(Seller, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/seller")
    public void createSeller(@RequestBody Seller seller, @RequestHeader("session-id") String key,
            @RequestHeader("aid") String aid) {
        if (!(Session.isValid("admin", aid, key)))
            return;
        DatabaseWrapper.addSeller(seller);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/seller/{sid}")
    public void updateSeller(@PathVariable String sid, @RequestHeader("session-id") String key,
            @RequestHeader("aid") String aid) {
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/seller/{sid}")
    public void deleteProduct(@PathVariable String sid, @RequestHeader("session-id") String key,
            @RequestHeader("aid") String aid) {
        if (!(Session.isValid("admin", aid, key)))
            return;
        DatabaseWrapper.deleteUser("Sellers", sid);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/seller/ban/{sid}")
    public void banUser(@PathVariable String sid, @RequestHeader("session-id") String key,
            @RequestHeader("aid") String aid) throws InterruptedException {
        if (!(Session.isValid("admin", aid, key)))
            return;
        Seller seller = (Seller) DatabaseWrapper.getUser("sellers", sid);
        seller.setStatus("banned");
        DatabaseWrapper.addSeller(seller);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/seller/unban/{sid}")
    public void unbanUser(@PathVariable String sid, @RequestHeader("session-id") String key,
            @RequestHeader("aid") String aid) throws InterruptedException {
        if (!(Session.isValid("admin", aid, key)))
            return;
        Seller seller = (Seller) DatabaseWrapper.getUser("sellers", sid);
        seller.setStatus("normal");
        DatabaseWrapper.addSeller(seller);
    }

}