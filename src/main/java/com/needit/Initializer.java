package com.needit;

import com.google.firebase.FirebaseOptions;
import com.needit.config.Session;
import com.needit.model.Admin;
import com.needit.model.Clothing;
import com.needit.model.Customer;
import com.needit.model.Instrument;
import com.needit.model.Product;
import com.needit.model.Seller;
import com.needit.model.Technology;
import com.needit.model.User;
import com.needit.utility.DatabaseWrapper;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.ApplicationContext;
import org.springframework.boot.SpringApplication;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import org.springframework.core.io.ClassPathResource;

@SpringBootApplication
public class Initializer {

    public Initializer() {
    }

    public static void main(String[] args) throws FileNotFoundException, IOException, InterruptedException {
        SpringApplication.run(Initializer.class, args);
        FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(
                        GoogleCredentials.fromStream(new ClassPathResource("/serviceAccountKey.json").getInputStream()))
                .setDatabaseUrl("https://needit-46c75.firebaseio.com/").build();

        FirebaseApp.initializeApp(options);

        List<Instrument> insts = (List<Instrument>) (List<? extends Product>) DatabaseWrapper.getProducts("instrument");
        List<Technology> techs = (List<Technology>) (List<? extends Product>) DatabaseWrapper.getProducts("technology");
        List<Clothing> cloths = (List<Clothing>) (List<? extends Product>) DatabaseWrapper.getProducts("clothing");

        /*
         * Gson gson = new Gson(); JsonReader reader = new JsonReader(new FileReader(new
         * ClassPathResource("/products_tech.json").getFile()));
         * 
         * Technology[] products_t = gson.fromJson(reader, Technology[].class);
         * List<Technology> asList_t = Arrays.asList(products_t); for(Technology product
         * : asList_t){ product.setStock(10); DatabaseWrapper.addTechnology(product); }
         * gson = new Gson(); reader = new JsonReader(new FileReader(new
         * ClassPathResource("/products_cloth.json").getFile())); Clothing[] products_c
         * = gson.fromJson(reader, Clothing[].class); List<Clothing> asList_c =
         * Arrays.asList(products_c); for(Clothing product : asList_c){
         * product.setStock(10); DatabaseWrapper.addClothing(product); }
         * 
         * gson = new Gson(); reader = new JsonReader(new FileReader(new
         * ClassPathResource("/products_inst.json").getFile())); Instrument[]
         * products_i= gson.fromJson(reader, Instrument[].class); List<Instrument>
         * asList_i = Arrays.asList(products_i); for(Instrument product : asList_i){
         * product.setStock(10); DatabaseWrapper.addInstrument(product); }
         */
        Customer cus1 = new Customer("ctahasevim@gmail.com", "12345678c", "Taha", "Sevim");
        Customer cus2 = new Customer("cselcukguvel@gmail.com", "12345678c", "Selcuk", "Guvel");
        Customer cus3 = new Customer("cnursenauzar@gmail.com", "12345678c", "Nursena", "Uzar");
        Seller seller1 = new Seller("stahasevim@gmail.com", "12345678s", "Taha", "Sevim", "Boyner");
        Seller seller2 = new Seller("sselcukguvel@gmail.com", "12345678s", "Selcuk", "Guvel", "Boyner");
        Seller seller3 = new Seller("snursenauzar@gmail.com", "12345678s", "Nursena", "Uzar", "Boyner");

        for (int i = 0; i < insts.size() - 3; i = i + 3) {
            cus1.addToCard(insts.get(i));
            cus2.addToCard(insts.get(i + 1));
            cus3.addToCard(insts.get(i + 2));
            seller1.addToProductIds(insts.get(i).getId(), 1);
            seller2.addToProductIds(insts.get(i + 1).getId(), 1);
            seller3.addToProductIds(insts.get(i + 2).getId(), 1);
        }

        for (int i = 0; i < techs.size() - 3; i = i + 3) {
            cus1.addToCard(techs.get(i));
            cus2.addToCard(techs.get(i + 1));
            cus3.addToCard(techs.get(i + 2));
            seller1.addToProductIds(techs.get(i).getId(), 1);
            seller2.addToProductIds(techs.get(i + 1).getId(), 1);
            seller3.addToProductIds(techs.get(i + 2).getId(), 1);
        }
        for (int i = 0; i < cloths.size() - 3; i = i + 3) {
            cus1.addToCard(cloths.get(i));
            cus2.addToCard(cloths.get(i + 1));
            cus3.addToCard(cloths.get(i + 2));
            seller1.addToProductIds(cloths.get(i).getId(), 1);
            seller2.addToProductIds(cloths.get(i + 1).getId(), 1);
            seller3.addToProductIds(cloths.get(i + 2).getId(), 1);
        }

        /*
         * DatabaseWrapper.addCustomer(cus1); DatabaseWrapper.addCustomer(cus2);
         * DatabaseWrapper.addCustomer(cus3); DatabaseWrapper.addSeller(seller1);
         * DatabaseWrapper.addSeller(seller2); DatabaseWrapper.addSeller(seller3);
         * DatabaseWrapper.addAdmin(new Admin("admin1@gmail.com", "admin1admin1",
         * "admin1name", "admin1surname"));
         */
        // Datas in below are already in firebase database

        // Loading products from json

    }

    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return args -> {
            /*
             * Stream.of("Denver JUG", "Utah JUG", "Seattle JUG", "Richmond JUG")
             * .forEach(name -> repository.save(new Group(name)));
             * 
             * Group djug = repository.findByName("Denver JUG"); Event e =
             * Event.builder().title("Full Stack Reactive").
             * description("Reactive with Spring Boot + React")
             * .date(Instant.parse("2018-12-12T18:00:00.000Z")).build();
             * djug.setEvents(Collections.singleton(e)); repository.save(djug);
             * 
             * repository.findAll().forEach(System.out::println);
             */
        };
    }

}