package com.needit.model;

public class Clothing extends Product {

    private String size;
    private String gender;

    public Clothing(){}
    public Clothing(String name, String brand, double price, String imageUrl, String information, 
        String color, String sellerId, int stock, String size, String gender){
        super(name, brand, price, imageUrl, information, color, sellerId, stock);
        this.size = size;
        this.gender = gender;

    }

    
    @Override
    public String toString(){
        return "clothing";
    }

    /**
     * @return the size
     */
    public String getSize() {
        return size;
    }

    /**
     * @param size the size to set
     */
    public void setSize(String size) {
        this.size = size;
    }

    /**
     * @return the gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender the gender to set
     */
    public void setGender(String gender) {
        this.gender = gender;
    }
}