package com.needit.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.needit.utility.DatabaseWrapper;

public class Card {

    private HashMap<String, Integer> productIDs;
    private double price;

    public Card(){
        this.productIDs = new HashMap<String, Integer>();
        this.price = 0;
    }

    public void removeProduct(String category, String pid) throws InterruptedException {
        Product p = DatabaseWrapper.getProduct(category, pid);
        if (p == null){
            System.out.println("pid:"+ pid + " icin null");
            return;
        }
        int curr = productIDs.get(pid);
        this.price -= (curr*p.getPrice());
        productIDs.remove(pid);
    }

    public void updateQty(String category, String pid, int qty) throws InterruptedException {
        Product p = DatabaseWrapper.getProduct(category, pid);
        if(!(productIDs.containsKey(pid))){
            productIDs.put(pid, qty);
            this.price += (qty*p.getPrice());
            return;
        }
        int curr = productIDs.get(pid);
        this.price += (qty*p.getPrice())-(curr*p.getPrice());
        productIDs.put(pid, qty);
    }
    public void addToProductIDs(Product item) {
        String id = item.getId();
        if (productIDs.containsKey(id)){
            int count = productIDs.get(id);
            productIDs.put(id, count+1);
        }else
            productIDs.put(id, 1);
        price += item.getPrice();
    }

    public void addToProductIDs(String pid) throws InterruptedException {
        Product p  = DatabaseWrapper.getProduct("instrument", pid);
        if (p != null){
            this.addToProductIDs(p);
            return;
        }
        p = DatabaseWrapper.getProduct("technology", pid);
        if (p != null){
            this.addToProductIDs(p);
            return;
        } 
        p = DatabaseWrapper.getProduct("clothing", pid);
        if (p != null){
            this.addToProductIDs(p);
            return;
        }
    }

    /**
     * @return the productIDs
     */
    public HashMap<String, Integer> getProductIDs() {
        return productIDs;
    }
    /**
     * @return the price
     */
    public double getPrice() {
        return price;
    }

    /**
     * @param productIDs the productIDs to set
     */
    public void setProductIDs(HashMap<String, Integer> productIDs) {
        this.productIDs = productIDs;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(double price) {
        this.price = price;
    }
    
}