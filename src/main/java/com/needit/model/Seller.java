package com.needit.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Seller extends User{

    private String company;
    private int totalProduct;
    private HashMap<String, Integer> productIDs;
    
    public Seller(){};
    public Seller(String email, String password, String name, String surname, String company){
        super(email, password, name, surname, "seller");
        this.productIDs = new HashMap<String, Integer>();
        this.company = company;
    }

    public void addToProductIds(String id, int stock){
        productIDs.put(id, stock);
        totalProduct += stock;
    }
    /**
     * @return the company
     */
    public String getCompany() {
        return company;
    }
    /**
     * @param company the company to set
     */
    public void setCompany(String company) {
        this.company = company;
    }
   /**
     * @return the totalProduct
     */
    public int getTotalProduct() {
        return totalProduct;
    }


    /**
     * @return the productIDs
     */
    public HashMap<String, Integer> getProductIDs() {
        return productIDs;
    }

    /**
     * @param productIDs the productIDs to set
     */
    public void setProductIDs(HashMap<String, Integer> productIDs) {
        this.productIDs = productIDs;
    }

    /**
     * @param totalProduct the totalProduct to set
     */
    public void setTotalProduct(int totalProduct) {
        this.totalProduct = totalProduct;
    }


}