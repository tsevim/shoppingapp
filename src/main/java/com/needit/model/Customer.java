package com.needit.model;

public class Customer extends User{

    
    private Card shoppingCard;

    public Customer(){}
    public Customer(String email, String password, String name, String surname){
        super(email, password, name, surname, "customer");
        this.shoppingCard = new Card();
    }

    public Customer(String email, String password, String name, String surname, Card shoppingCard){
        super(email, password, name, surname, "customer");
        this.shoppingCard = shoppingCard;
    }
    /**
     * @return the shoppingCard
     */
    public Card getShoppingCard() {
        return shoppingCard;
    }

    public void addToCard(Product item){
        shoppingCard.addToProductIDs(item);
    }

    /**
     * @param shoppingCard the shoppingCard to set
     */
    public void setShoppingCard(Card shoppingCard) {
        this.shoppingCard = shoppingCard;
    }
    
}