package com.needit.model;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class Product {

    @Id
    private String id;
    private String name;
    private String brand;
    private double price;
    private String imageUrl;
    private String information;
    private String color;
    private String sellerId;
    private int stock;

    public Product() {
        this.id = UUID.randomUUID().toString().replace("-", "").substring(0, 16);
    }

    public Product(String name, String brand, double price, String imageUrl, String information, String color,
            String sellerId, int stock) {
        this();
        this.name = name;
        this.brand = brand;
        this.price = price;
        this.imageUrl = imageUrl;
        this.information = information;
        this.color = color;
        this.sellerId = sellerId;
        this.stock = stock;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @return the price
     */
    public double getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * @return the imageUrl
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     * @param imageUrl the imageUrl to set
     */
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the imageUrl to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the brand
     */
    public String getBrand() {
        return brand;
    }

    /**
     * @param brand the brand to set
     */
    public void setBrand(String brand) {
        this.brand = brand;
    }

    /**
     * @return the information
     */
    public String getInformation() {
        return information;
    }

    /**
     * @param information the brand to set
     */
    public void setInformation(String information) {
        this.information = information;
    }

    /**
     * @return the sellerId
     */
    public String getColor() {
        return color;
    }

    /**
     * @param color to set
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * @return sellerId the subCategory
     */
    public String getSellerId() {
        return sellerId;
    }

    /**
     * @param sellerId the brand to set
     */
    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

}