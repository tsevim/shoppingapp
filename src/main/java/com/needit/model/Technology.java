package com.needit.model;

public class Technology extends Product{

    private String device;
    private String additionalInfo;

    public Technology(){}
    public Technology(String name, String brand, double price, String imageUrl, String information, 
        String color, String sellerId, int stock, String device, String additionalInfo){
        super(name, brand, price, imageUrl, information, color, sellerId, stock);
        this.device = device;
        this.additionalInfo = additionalInfo;
    }

    
    @Override
    public String toString(){
        return "technology";
    }

    /**
     * @return the device
     */
    public String getDevice() {
        return device;
    }

    /**
     * @param device the device to set
     */
    public void setDevice(String device) {
        this.device = device;
    }

    /**
     * @return the additionalInfo
     */
    public String getAdditionalInfo() {
        return additionalInfo;
    }

    /**
     * @param additionalInfo the additionalInfo to set
     */
    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }
}