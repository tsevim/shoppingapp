package com.needit.model;

public class Instrument extends Product{

    private String type;
    private boolean isElectro;
    private String woodType;

    public Instrument(){}
    public Instrument(String name, String brand, double price, String imageUrl, String information, 
        String color, String sellerId, int stock, String type, boolean isElectro, String woodType){
        super(name, brand, price, imageUrl, information, color, sellerId, stock);
        this.type = type;
        this.isElectro = isElectro;
        this.woodType = woodType;
        
    }


    @Override
    public String toString(){
        return "instrument";
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the isElectro
     */
    public boolean getIsElectro() {
        return isElectro;
    }

    /**
     * @param isElectro the isElectro to set
     */
    public void setElectro(boolean isElectro) {
        this.isElectro = isElectro;
    }

    /**
     * @return the woodType
     */
    public String getWoodType() {
        return woodType;
    }

    /**
     * @param woodType the woodType to set
     */
    public void setWoodType(String woodType) {
        this.woodType = woodType;
    }

    
}