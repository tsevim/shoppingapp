package com.needit.config;

import java.util.HashMap;
import java.util.UUID;

public class Session {

    private static HashMap<String, String> customerSessions = new HashMap<String, String>();
    private static HashMap<String, String> sellerSessions = new HashMap<String, String>();
    private static HashMap<String, String> adminSessions = new HashMap<String, String>();

    private static boolean DEBUG = true;

    public static void addSessionKey(String type, String id, String key) {
        if (type.equals("customer"))
            customerSessions.put(id, key);
        else if (type.equals("seller"))
            sellerSessions.put(id, key);
        else if (type.equals("admin"))
            adminSessions.put(id, key);
        System.out.println("Customers: " + customerSessions);
        System.out.println("Sellers: " + sellerSessions);
        System.out.println("Admins: " + adminSessions);

    }

    public static boolean isValid(String type, String id, String key) {
        System.out.println("Customers: " + customerSessions);
        System.out.println("Sellers: " + sellerSessions);
        System.out.println("Admins: " + adminSessions);
        if (DEBUG)
            return true;
        if (type.equals("customer"))
            return customerSessions.containsKey(id) && customerSessions.get(id).equals(key);
        else if (type.equals("admin"))
            return adminSessions.containsKey(id) && adminSessions.get(id).equals(key);
        return sellerSessions.containsKey(id) && sellerSessions.get(id).equals(key);

    }

    public static void removeSessionKey(String type, String id) {
        if (type.equals("customer"))
            customerSessions.remove(id);
        else if (type.equals("seller"))
            sellerSessions.remove(id);
        else if (type.equals("admin"))
            adminSessions.remove(id);
    }

    public static String generateSessionKey() {
        return UUID.randomUUID().toString().replace("-", "");
    }
}