package com.needit.utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;
import com.needit.model.Customer;
import com.needit.model.Instrument;
import com.needit.model.Product;
import com.needit.model.Seller;
import com.needit.model.Technology;
import com.needit.model.User;
import com.needit.model.Admin;
import com.needit.model.Card;
import com.needit.model.Clothing;



public class DatabaseWrapper {

    private final static DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
    private final static DatabaseReference usersRef = ref.child("users");
    private final static DatabaseReference productsRef = ref.child("products");
    private static Object retrieverObject = null;

    public static void updateCustomerCard(String id, Card card){
        usersRef.child("customers").child(id).child("shoppingCard").setValueAsync(card);
    }
    
    public static void updateSellerProducts(String id, HashMap<String, Integer> productIDs){
        usersRef.child("sellers").child(id).child("productIDs").setValueAsync(productIDs);
    }

    public static User getUser(String type, String id) throws InterruptedException {
        CountDownLatch done = new CountDownLatch(1);
        usersRef.child(type).child(id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (type.equals("customers"))
                    retrieverObject = dataSnapshot.getValue(Customer.class);
                else if(type.equals("sellers"))
                   retrieverObject = dataSnapshot.getValue(Seller.class);
                else if(type.equals("admins"))
                    retrieverObject = dataSnapshot.getValue(Admin.class);
                done.countDown();
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
              System.out.println("The read failed: " + databaseError.getCode());
            }
          });
        done.await();
        return (User)retrieverObject;
    }
    
    public static List<User> getAllUsers(String type) throws InterruptedException {
        CountDownLatch done = new CountDownLatch(1);
        usersRef.child(type).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<User> userList = new ArrayList<User>();
                Iterable<DataSnapshot> snaps =  dataSnapshot.getChildren();
                for(DataSnapshot ds : snaps){
                    if (type.equals("customers"))
                        userList.add(ds.getValue(Customer.class));
                    else if(type.equals("sellers"))
                        userList.add(ds.getValue(Seller.class));
                    else if(type.equals("admins"))
                        userList.add(ds.getValue(Admin.class));
                }
                retrieverObject = userList;
                done.countDown();
            }
          
            @Override
            public void onCancelled(DatabaseError databaseError) {
              System.out.println("The read failed: " + databaseError.getCode());
            }
          });
        done.await();
        return (List<User>)retrieverObject;
    }
    
    public static void addCustomer(Customer customer){
        usersRef.child("customers").child(customer.getId()).setValueAsync(customer);
    }

    public static void addSeller(Seller seller){
        usersRef.child("sellers").child(seller.getId()).setValueAsync(seller);
    }

    public static void addAdmin(Admin admin){
        usersRef.child("admins").child(admin.getId()).setValueAsync(admin);
    }

    public static void deleteUser(String type, String id){
        usersRef.child(type).child(id).removeValueAsync();
    }

    public static List<Product> getProducts(String category) throws InterruptedException {
        CountDownLatch done = new CountDownLatch(1);
        productsRef.child(category).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> data =  dataSnapshot.getChildren();
                List<Product> productList = new ArrayList<Product>();
                for(DataSnapshot ds : data){
                    if (category.equals("technology"))
                        productList.add(ds.getValue(Technology.class));
                    else if(category.equals("instrument"))
                        productList.add(ds.getValue(Instrument.class));
                    else if(category.equals("clothing"))
                        productList.add(ds.getValue(Clothing.class));
                }

                retrieverObject = productList;
                done.countDown();

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
              System.out.println("The read failed: " + databaseError.getCode());
            }
          });
        done.await();
        return (List<Product>)retrieverObject;
    }

    public static Product getProduct(String category, String id) throws InterruptedException {
        CountDownLatch done = new CountDownLatch(1);
        productsRef.child(category).child(id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (category.equals("instrument"))
                    retrieverObject = dataSnapshot.getValue(Instrument.class);
                else if(category.equals("clothing"))
                    retrieverObject = dataSnapshot.getValue(Clothing.class);
                else if(category.equals("technology"))
                    retrieverObject = dataSnapshot.getValue(Technology.class);

                done.countDown();
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
              System.out.println("The read failed: " + databaseError.getCode());
            }
          });
        done.await();
        return (Product)retrieverObject;
    }

    public static void addInstrument(Instrument product){
        productsRef.child(product.toString()).child(product.getId()).setValueAsync(product);
    }

    public static void addTechnology(Technology product){
        System.out.println(product.getId());
        productsRef.child(product.toString()).child(product.getId()).setValueAsync(product);
    }

    public static void addClothing(Clothing product){
        productsRef.child(product.toString()).child(product.getId()).setValueAsync(product);
    }

    public static void deleteProduct(String category, String id){
        productsRef.child(category).child(id).removeValueAsync();
    }
}