# Online Shopping System

## Branch name convention

### `<type>/<name>`

#### `<type>`

```
bug    - Code changes linked to a known issue.
feat   - New feature.
hotfix - Quick fixes to the codebase.
junk   - Experiments (will never be merged).
...
```

#### `<name>`

Always use dashes to seperate words, and keep it short.

#### Example branch names

```
feat/renderer-cookies
hotfix/dockerfile-base-image
bug/login-ie
```

## Commit convention

### `<verb> <object>`

#### `<verb>`

Write the summary line and description of what you have done in the imperative mode, that is as if you were commanding someone. Start the line with "Fix", "Add", "Change" instead of "Fixed", "Added", "Changed".

```
Fix
Add
Update
...
```

#### `<object>`

The object that is added, updated, fixed etc..

#### Example commits

```
Fix failing UserControllerTests
Add tests for UserController
Update docbook dependency and add product to shopping cart
```

## Setting Development Environment

### Prerequisites

#### Install Yarn

https://yarnpkg.com/lang/en/docs/install/#debian-stable

### Install repository

To install the repository run the following commands:

```bash
git clone https://github.com/selcukguvel/bbm384s2019g19
cd bbm384s2019g19
```

This will get a copy of the project installed locally. To install all of its dependencies and start each app, follow the instructions below.

To run the server, run:

```bash
./mvnw spring-boot:run
```

To run the client, cd into the `app` folder and run:

```bash
yarn && yarn start
```

### Notes

To solve error: "An exception occurred while running. null: InvocationTargetException: Connector configured to listen on port 8080 failed to start..."

Run: (to kill the process on some port)

```bash
sudo kill `sudo lsof -t -i:port_number`
```

For our case, it is:

```bash
sudo kill `sudo lsof -t -i:8080`
```

To resolve error "Servlet.service() for servlet [dispatcherServlet] in context with path [] ..."

Run:

```bash
./mvnw clean spring-boot:run
```

to start server.
