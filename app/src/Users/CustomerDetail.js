import React, { Component } from "react";
import {
  Card,
  Row,
  Col,
  Container,
  Button,
  FormControl,
  FormLabel,
  FormGroup
} from "react-bootstrap";

import { Link } from "react-router-dom";
import "../assets/Product/ProductDetailed.css";

import NavigationBar from "../NavBar/NavigationBar";
import SearchBar from "../SearchBar/SearchBar";
import backLogo from "../assets/images/back.png";

export default class CustomerDetail extends Component {
  constructor(props) {
    super(props);
    this.state = { user: this.props.location.user };
  }

  banUser = () => {
    var user = this.state.user;
    console.log("banUser" + user);
    var bannedUserId = user.id;
    fetch("/api/customer/ban/" + bannedUserId, {
      method: "POST",
      headers: {
        "session-id": this.props.location.userInfo.sessionId,
        aid: this.props.location.userInfo.userId,
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    });
    user.status = "banned";
    this.setState({ user: user });
  };

  unbanUser = () => {
    var user = this.state.user;
    console.log("unbanUser" + user);
    var bannedUserId = user.id;
    fetch("/api/customer/unban/" + bannedUserId, {
      method: "POST",
      headers: {
        "session-id": this.props.location.userInfo.sessionId,
        aid: this.props.location.userInfo.userId,
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    });
    user.status = "normal";
    this.setState({ user: user });
  };

  render() {
    console.log(this.state.user);
    console.log("CustomerDetail", this.props);
    console.log("prevPath", this.props.location.prevpath);
    return (
      <div>
        <NavigationBar
          mode="admin"
          auth={this.props.location.auth}
          userInfo={this.props.location.userInfo}
          history={this.props.history}
          products={this.props.location.products}
        />
        <Link
          to={{
            pathname: this.props.location.prevpath,
            sellers: this.props.location.sellers,
            customers: this.props.location.customers,
            userInfo: this.props.location.userInfo,
            auth: this.props.location.auth,
            products: this.props.location.products
          }}
        >
          <div className="back-logo">
            <Card.Img variant="top" className="img" src={backLogo} />
          </div>
        </Link>
        <div className="Register">
          <form>
            <FormGroup controlId="name" bsSize="large">
              <FormLabel className="Label">Name</FormLabel>
              <FormControl
                className="Input"
                autoFocus
                type="text"
                value={this.state.user.name}
                disabled
              />
            </FormGroup>
            <FormGroup controlId="surname" bsSize="large">
              <FormLabel className="Label">Surname</FormLabel>
              <FormControl
                className="Input"
                autoFocus
                type="text"
                disabled
                value={this.state.user.surname}
              />
            </FormGroup>
            <FormGroup controlId="email" bsSize="large">
              <FormLabel className="Label">Email</FormLabel>
              <FormControl
                className="Input"
                autoFocus
                type="email"
                disabled
                value={this.state.user.email}
              />
            </FormGroup>
            <FormGroup controlId="accountType">
              <FormLabel>Account Type</FormLabel>
              <FormControl as="select" disabled value={this.state.user.type}>
                <option value="customer">Customer</option>
                <option value="seller">Seller</option>
              </FormControl>
            </FormGroup>
            <Button
              onClick={
                this.state.user.status == "normal"
                  ? this.banUser
                  : this.unbanUser
              }
              block
              variant={
                this.state.user.status == "normal" ? "danger" : "success"
              }
            >
              {this.state.user.status == "normal" ? (
                <a>Ban User</a>
              ) : (
                <a>Unban User</a>
              )}
            </Button>
          </form>
        </div>
      </div>
    );
  }
}
