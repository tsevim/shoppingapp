import React, { Component } from "react";
import { Card, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import userLogo from "../assets/images/userPhoto.png";
import "../assets/Product/Product.css";

export default class UserView extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    var { user } = this.props;
    var toChild = {
      pathname: "/" + user.type + "s/" + user.id,
      prevpath: "/" + user.type + "s",
      sellers: this.props.sellers,
      customers: this.props.customers,
      user: user,
      userInfo: this.props.userInfo,
      auth: this.props.auth,
      products: this.props.products
    };
    return (
      <Card style={{ width: "18rem" }} className="product">
        <div className="img-wrapper">
          <Card.Img variant="top" className="img" src={userLogo} />
        </div>
        <Card.Body>
          <Card.Title className="ellipsis">
            {" "}
            {user.name.charAt(0).toUpperCase()}. {user.surname}
          </Card.Title>
          <Card.Text className="ellipsis">{user.information}</Card.Text>
        </Card.Body>
        <Link to={toChild}>
          <Button block variant="dark">
            {" "}
            Details{" "}
          </Button>
        </Link>
      </Card>
    );
  }
}
