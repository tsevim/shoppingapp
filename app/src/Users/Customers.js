import React from "react";
import UserView from "./UserView";
import { Row, Container } from "react-bootstrap";
import "../assets/Product/Product.css";
import NavigationBar from "../NavBar/NavigationBar";
import AdminLeftBar from "../HomePage/AdminLeftBar";

export default class Customers extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      sellers: this.props.location.sellers,
      customers: this.props.location.customers
    };
  }

  render() {
    console.log("CUSTOMERRRRRR", this.props.location.products);
    console.log("customers", this.state);
    return (
      <div>
        <NavigationBar
          mode="admin"
          auth={this.props.location.auth}
          userInfo={this.props.location.userInfo}
          history={this.props.history}
          products={this.props.location.products}
        />
        <AdminLeftBar
          sellers={this.state.sellers}
          customers={this.state.customers}
          auth={this.props.location.auth}
          userInfo={this.props.location.userInfo}
          products={this.props.location.products}
        />
        <Container>
          <Row style={{ margin: "0 auto" }}>
            {this.state.customers.map(customer => (
              <UserView
                sellers={this.state.sellers}
                customers={this.state.customers}
                key={customer.id}
                user={customer}
                auth={this.props.location.auth}
                userInfo={this.props.location.userInfo}
                products={this.props.location.products}
              />
            ))}
          </Row>
        </Container>
      </div>
    );
  }
}
