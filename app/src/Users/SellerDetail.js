import React, { Component } from "react";
import {
  Card,
  Row,
  Col,
  Container,
  Button,
  FormControl,
  FormLabel,
  FormGroup,
  Modal
} from "react-bootstrap";

import { Link } from "react-router-dom";
import "../assets/Product/ProductDetailed.css";
import ProductList from "../Product/ProductList";

import NavigationBar from "../NavBar/NavigationBar";
import SearchBar from "../SearchBar/SearchBar";
import backLogo from "../assets/images/back.png";

export default class SellerDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: this.props.location.user,
      sellerProducts: [],
      showModal: false
    };

    var products = this.props.location.products;
    var sellersProductIDs = this.state.user.productIDs;
    var sellerProducts = [];
    for (var i in products) {
      var productId = products[i].id;
      for (var id in sellersProductIDs) {
        if (productId == id) {
          sellerProducts.push(products[i]);
          break;
        }
      }
    }
    this.state.sellerProducts = sellerProducts;
  }

  banUser = () => {
    var user = this.state.user;
    console.log("banUser" + user);
    var bannedUserId = user.id;
    fetch("/api/seller/ban/" + bannedUserId, {
      method: "POST",
      headers: {
        "session-id": this.props.location.userInfo.sessionId,
        aid: this.props.location.userInfo.userId,
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    });
    user.status = "banned";
    this.setState({ user: user });
  };

  unbanUser = () => {
    var user = this.state.user;
    console.log("unbanUser" + user);
    var bannedUserId = user.id;
    fetch("/api/seller/unban/" + bannedUserId, {
      method: "POST",
      headers: {
        "session-id": this.props.location.userInfo.sessionId,
        aid: this.props.location.userInfo.userId,
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    });
    user.status = "normal";
    this.setState({ user: user });
  };

  showSellerProducts = () => {
    console.log("showSellerProducts");
    console.log(this.state.sellerProducts);
    this.setState({ showModal: true });
  };

  hideSellerProducts = () => {
    this.setState({ showModal: false });
  };

  render() {
    console.log(this.state.showModal);
    console.log(this.state.user);
    console.log("SellerDetail", this.props);
    console.log("prevPath", this.props.location.prevpath);
    return (
      <div>
        <NavigationBar
          mode="admin"
          auth={this.props.location.auth}
          userInfo={this.props.location.userInfo}
          history={this.props.history}
          products={this.props.location.products}
        />
        <Link
          to={{
            pathname: this.props.location.prevpath,
            sellers: this.props.location.sellers,
            customers: this.props.location.customers,
            userInfo: this.props.location.userInfo,
            auth: this.props.location.auth,
            products: this.props.location.products
          }}
        >
          <div className="back-logo">
            <Card.Img variant="top" className="img" src={backLogo} />
          </div>
        </Link>
        <div className="Register">
          <form>
            <FormGroup controlId="name" bsSize="large">
              <FormLabel className="Label">Name</FormLabel>
              <FormControl
                className="Input"
                autoFocus
                type="text"
                value={this.state.user.name}
                disabled
              />
            </FormGroup>
            <FormGroup controlId="surname" bsSize="large">
              <FormLabel className="Label">Surname</FormLabel>
              <FormControl
                className="Input"
                autoFocus
                type="text"
                disabled
                value={this.state.user.surname}
              />
            </FormGroup>
            <FormGroup controlId="email" bsSize="large">
              <FormLabel className="Label">Email</FormLabel>
              <FormControl
                className="Input"
                autoFocus
                type="email"
                disabled
                value={this.state.user.email}
              />
            </FormGroup>
            <FormGroup controlId="accountType">
              <FormLabel>Account Type</FormLabel>
              <FormControl as="select" disabled value={this.state.user.type}>
                <option value="customer">Customer</option>
                <option value="seller">Seller</option>
              </FormControl>
            </FormGroup>
            <Button onClick={this.showSellerProducts} variant="dark" block>
              Products
            </Button>
            <Modal
              show={this.state.showModal}
              onHide={this.hideSellerProducts}
              size="lg"
              centered
            >
              <Modal.Header closeButton>
                <Modal.Title>
                  {this.state.user.name} {this.state.user.surname}'s Products
                </Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <div>
                  <Container>
                    <ProductList
                      mode="preview"
                      products={this.state.sellerProducts}
                      auth={this.props.location.auth}
                      userInfo={this.props.location.userInfo}
                    />
                  </Container>
                </div>
              </Modal.Body>
            </Modal>

            <Button
              onClick={
                this.state.user.status == "normal"
                  ? this.banUser
                  : this.unbanUser
              }
              block
              variant={
                this.state.user.status == "normal" ? "danger" : "success"
              }
            >
              {this.state.user.status == "normal" ? (
                <a>Ban User</a>
              ) : (
                <a>Unban User</a>
              )}
            </Button>
          </form>
        </div>
      </div>
    );
  }
}
