import React, { Component } from "react";
import { Navbar, Nav, NavDropdown } from "react-bootstrap";
import "../assets/Home/Home.css";
import cartIcon from "../assets/images/cart.png";
import userIcon from "../assets/images/user.png";
import { NavLink, Link } from "react-router-dom";

export default class NavigationBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      auth: this.props.auth,
      mode: this.props.mode,
      homeButtonRedirect: ""
    };

    if (this.props.mode == "admin")
      this.state.homeButtonRedirect = "/adminpanel";
    else if (this.props.mode == "seller")
      this.state.homeButtonRedirect = "/sellerpanel";
    else this.state.homeButtonRedirect = "/home";
  }

  handleOpen = () => {
    this.setState({ isOpen: true });
  };

  logOut = async () => {
    var userInfo = this.props.userInfo;
    fetch("/api/logout", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "session-id": userInfo.sessionId
      },
      body: JSON.stringify({
        id: userInfo.userId,
        type: "customer"
      })
    });
    this.state.auth = false;
    window.location = "/home";
  };

  editAccount = () => {
    this.props.history.push({
      pathname: "/account",
      userInfo: this.props.userInfo,
      mode: this.props.mode,
      auth: true,
      products: this.props.products
    });
  };

  render() {
    //console.log("NavBar");
    //console.log(this.props.auth);
    console.log("navprops", this.props);
    if (this.state.auth) {
      return (
        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
          <Link
            to={{
              pathname: this.state.homeButtonRedirect,
              userInfo: this.props.userInfo,
              auth: this.props.auth,
              products: this.props.products,
              retrieveAllProducts: "true"
            }}
          >
            <Navbar.Brand className="brand">
              <strong>needit</strong>
            </Navbar.Brand>
          </Link>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mr-auto" />
            <Nav className="right-container">
              <img src={userIcon} className="cart" />
              <NavDropdown
                onClick={this.handleOpen}
                open={this.state.isOpen}
                title="Account"
              >
                <NavDropdown.Item onClick={this.editAccount}>
                  Edit
                </NavDropdown.Item>
                <NavDropdown.Item onClick={this.logOut}>
                  Logout
                </NavDropdown.Item>
              </NavDropdown>
              <div
                style={{
                  display:
                    this.state.mode == "admin" || this.state.mode == "seller"
                      ? "none"
                      : true
                }}
              >
                <NavLink
                  to={{
                    pathname: "/cart",
                    userInfo: this.props.userInfo,
                    auth: this.props,
                    products: this.props.products
                  }}
                  style={{ color: "rgb(173, 187, 206)" }}
                >
                  <img src={cartIcon} className="cart" /> Cart
                </NavLink>
              </div>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      );
    } else {
      return (
        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
          <Link
            to={{
              pathname: "/home",
              userInfo: this.props.userInfo,
              auth: this.props.auth,
              products: this.props.products,
              retrieveAllProducts: "true"
            }}
          >
            <Navbar.Brand className="brand">
              <strong>needit</strong>
            </Navbar.Brand>
          </Link>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mr-auto" />
            <Nav className="right-container">
              <NavLink
                to={{
                  pathname: "/",
                  userInfo: this.props.userInfo,
                  auth: this.props.auth
                }}
                style={{ color: "rgb(173, 187, 206)" }}
              >
                <img src={userIcon} className="cart" /> Sign in
              </NavLink>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      );
    }
  }
}
