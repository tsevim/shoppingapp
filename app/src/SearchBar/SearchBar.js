import React, { Component } from "react";
import {
  Button,
  FormControl,
  Form,
  Container,
  NavDropdown,
  ButtonGroup
} from "react-bootstrap";
import ProductList from "../Product/ProductList";

export default class SearchBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      filter: "Brand",
      searchWord: "",
      products: []
    };
  }

  search = async event => {
    var query = "?";
    var filter = this.state.filter;
    query += filter.toLowerCase() + "=" + this.state.searchWord;
    console.log(query);
    // if (isNameClicked) {
    //   query += "name=" + word;
    // }
    // if (isBrandClicked) {
    //   query += "brand=" + word;
    // }
    // if (isCategoryClicked) {
    //   query += "category=" + word;
    // }
    // if (query == "?") {
    //   query = "";
    // }
    const response = await fetch("/api/products" + query);
    const body = await response.json();
    console.log(body);
  };

  handleOpen = () => {
    this.setState({ isOpen: true });
  };

  updateFilter = event => {
    this.setState({ filter: event.target.id });
  };

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  };

  render() {
    console.log(this.state.products);
    return (
      <div>
        <Form inline>
          <div className="search">
            <FormControl
              type="text"
              placeholder="Search"
              className="search-bar"
              id="searchWord"
              onChange={this.handleChange}
            />
            <Button
              variant="secondary"
              onClick={this.search}
              className="search-button"
            >
              Search
            </Button>
            <NavDropdown
              onClick={this.handleOpen}
              open={this.state.isOpen}
              title={this.state.filter}
            >
              <NavDropdown.Item id="Brand" onClick={e => this.updateFilter(e)}>
                Brand
              </NavDropdown.Item>
              <NavDropdown.Item id="Name" onClick={e => this.updateFilter(e)}>
                Name
              </NavDropdown.Item>
              <NavDropdown.Item
                id="Category"
                onClick={e => this.updateFilter(e)}
              >
                Category
              </NavDropdown.Item>
            </NavDropdown>
          </div>
        </Form>
        <div>
          <Container>
            <ProductList
              mode="detailed"
              products={this.state.products}
              auth={this.props.auth}
              userInfo={this.props.userInfo}
            />
          </Container>
        </div>
      </div>
    );
  }
}
