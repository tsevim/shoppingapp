import React from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router";
import { Button } from "react-bootstrap";

const LinkButton = props => {
  const {
    type,
    history,
    location,
    match,
    staticContext,
    to,
    onClick,
    variant,
    // ⬆ filtering out props that `button` doesn’t know what to do with.
    ...rest
  } = props;

  const handleClick = async event => {
    event.preventDefault();
    console.log("handleClick");
    //this.setState({ isDisabled: true });
    const a = await onClick(event);
    console.log(a);
    if (a) {
      if (type == "register") {
        setTimeout(function() {
          history.push(to);
        }, 2000);
      } else {
        history.push(to);
      }
      //this.setState({ isDisabled: false });
    }
  };

  return (
    <Button
      {...rest} // `children` is just another prop!
      onClick={handleClick}
      variant={props.variant}
      block
    />
  );
};

LinkButton.propTypes = {
  to: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired
};

export default withRouter(LinkButton);
