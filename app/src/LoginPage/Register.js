import React, { Component } from "react";
import { FormGroup, FormControl, FormLabel, Dropdown } from "react-bootstrap";
import "../assets/Sign/Sign.css";
import LinkButton from "./LinkButton";
import NavigationBar from "../NavBar/NavigationBar";

export default class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "Name",
      surname: "Surname",
      email: "",
      password: "12345678",
      confirmPassword: "12345678",
      accountType: "customer",
      isPasswordMatchError: "none",
      isPasswordLengthError: "none",
      isEmailAtSymbolError: "none",
      isEmailTakenError: "none",
      isRegistrationSuccess: "none"
    };
    this.errors = {
      passwordMatchError: "Unmatched passwords!",
      passwordLengthError: "Passwords should be at least 8 characters!",
      emailAtSymbolError: "The email should contain @ symbol!",
      emailTakenError: "The email has already been taken by another user!"
    };

    this.successes = {
      registrationSuccess: "Registration is successful!"
    };
  }

  validateForm() {
    var isValid =
      this.state.name.length > 0 &&
      this.state.surname.length > 0 &&
      this.state.email.length > 0 &&
      this.state.password.length > 0 &&
      this.state.confirmPassword.length > 0;
    return isValid;
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  };

  async checkRegistration() {
    const { name, surname, email, password, accountType } = this.state;
    return fetch("/api/register", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        name: name,
        surname: surname,
        email: email,
        password: password,
        type: accountType.toLowerCase()
      })
    }).then(response => {
      if (!response.ok) {
        return false;
      } else {
        return true;
      }
    });
  }

  handleSubmit = async event => {
    event.preventDefault();
    const { email, password, confirmPassword, accountType } = this.state;
    /*const email = "taha@gmail.com";
    const password = "test1234";
    const confirmPassword = "test1234";*/
    if (!email.includes("@")) {
      console.log("The email should include @ symbol.");
      this.setState({
        isPasswordMatchError: "none",
        isPasswordLengthError: "none",
        isEmailAtSymbolError: true,
        isEmailTakenError: "none"
      });
      return false;
    } else if (password.length < 8) {
      console.log("The password length is less than 8.");
      this.setState({
        isPasswordMatchError: "none",
        isPasswordLengthError: true,
        isEmailAtSymbolError: "none",
        isEmailTakenError: "none"
      });
      return false;
    } else {
      if (password !== confirmPassword) {
        this.setState({
          isPasswordMatchError: true,
          isPasswordLengthError: "none",
          isEmailAtSymbolError: "none",
          isEmailTakenError: "none"
        });
        return false;
      } else {
        var responseJSON = await this.checkRegistration();
        if (responseJSON) {
          this.setState({ isRegistrationSuccess: true });
          console.log("Registration is successful");
          return true;
        } else {
          console.log("This email has been taken");
          this.setState({
            isPasswordMatchError: "none",
            isPasswordLengthError: "none",
            isEmailAtSymbolError: "none",
            isEmailTakenError: true
          });
          return false;
        }
      }
    }
  };

  render() {
    // Handle visibility of NavigationBar components
    return (
      <div>
        <NavigationBar auth={false} />
        <div className="Register">
          <form>
            <FormGroup controlId="name" bsSize="large">
              <FormLabel className="Label">Name</FormLabel>
              <FormControl
                className="Input"
                autoFocus
                type="text"
                value={this.state.name}
                onChange={this.handleChange}
              />
            </FormGroup>
            <FormGroup controlId="surname" bsSize="large">
              <FormLabel className="Label">Surname</FormLabel>
              <FormControl
                className="Input"
                autoFocus
                type="text"
                value={this.state.surname}
                onChange={this.handleChange}
              />
            </FormGroup>
            <FormGroup controlId="email" bsSize="large">
              <FormLabel className="Label">Email</FormLabel>
              <FormControl
                className="Input"
                autoFocus
                type="email"
                value={this.state.email}
                onChange={this.handleChange}
              />
            </FormGroup>
            <div
              style={{
                display: this.state.isEmailAtSymbolError,
                color: "red",
                marginTop: "-8px"
              }}
            >
              {this.errors.emailAtSymbolError}
            </div>
            <div
              style={{
                display: this.state.isEmailTakenError,
                color: "red",
                marginTop: "-8px"
              }}
            >
              {this.errors.emailTakenError}
            </div>
            <FormGroup controlId="password" bsSize="large">
              <FormLabel className="Label"> Password</FormLabel>
              <FormControl
                className="Input"
                value={this.state.password}
                onChange={this.handleChange}
                type="password"
              />
            </FormGroup>
            <div
              style={{
                display: this.state.isPasswordLengthError,
                color: "red",
                marginTop: "-8px"
              }}
            >
              {this.errors.passwordLengthError}
            </div>
            <FormGroup controlId="confirmPassword" bsSize="large">
              <FormLabel className="Label"> Confirm Password</FormLabel>
              <FormControl
                className="Input"
                value={this.state.confirmPassword}
                onChange={this.handleChange}
                type="password"
              />
            </FormGroup>
            <div
              style={{
                display: this.state.isPasswordMatchError,
                color: "red",
                marginTop: "-8px"
              }}
            >
              {this.errors.passwordMatchError}
            </div>
            <FormGroup controlId="accountType">
              <FormLabel>Account Type</FormLabel>
              <FormControl
                as="select"
                value={this.state.accountType}
                onChange={this.handleChange}
              >
                <option value="Customer">Customer</option>
                <option value="Seller">Seller</option>
              </FormControl>
            </FormGroup>
            <LinkButton
              type="register"
              to="/"
              onClick={this.handleSubmit}
              variant="dark"
              disabled={!this.validateForm()}
            >
              Sign up
            </LinkButton>
            <div
              style={{
                display: this.state.isRegistrationSuccess,
                color: "green",
                marginTop: "10px"
              }}
            >
              <b>{this.successes.registrationSuccess}</b>
            </div>
          </form>
        </div>
      </div>
    );
  }
}
