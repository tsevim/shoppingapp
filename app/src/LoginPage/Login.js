import React, { Component } from "react";
import { FormGroup, FormControl, FormLabel, FormText } from "react-bootstrap";
import "../assets/Sign/Sign.css";
//import { userService} from "./userService"; //WIP
import LinkButton from "./LinkButton";
import { Button } from "react-bootstrap";
import NavigationBar from "../NavBar/NavigationBar";

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "stahasevim@gmail.com",
      password: "12345678s",
      sessionId: "",
      userId: "",
      isLoginFailError: "none"
    };
    this.errors = {
      loginFailError: "Wrong email or password!"
    };
  }

  validateForm() {
    return this.state.email.length > 0 && this.state.password.length > 0;
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  };

  async checkAuthorization() {
    const { email, password } = this.state;
    console.log("checkAuth");
    console.log(email);
    console.log(password);
    return fetch("/api/login", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        email: email,
        password: password
      })
    }).then(response => {
      if (!response.ok) {
        console.log("test");
        return false;
      } else {
        console.log(response.headers);
        this.state.sessionId = response.headers.get("session-id");
        return response.json();
      }
    });
  }

  handleSubmit = async event => {
    event.preventDefault();
    // bool authorized = Encryp(password) == dict_database[email]
    // if(authorized) goto Homepage;
    // else alert('Wrong email or password');
    //const data = new FormData(event.target);
    var responseJSON = await this.checkAuthorization();
    const productResponse = await fetch("/api/products");
    const products = await productResponse.json();
    if (responseJSON) {
      console.log("userInfos", responseJSON);
      const { history } = this.props;
      this.state.userId = responseJSON.id;
      this.state.userType = responseJSON.type;
      const userInfos = {
        email: this.state.email,
        userId: this.state.userId,
        name: responseJSON.name,
        surname: responseJSON.surname,
        password: this.state.password,
        userType: this.state.userType,
        sessionId: this.state.sessionId
      };
      if (this.state.userType == "admin") {
        console.log("ADMINPANEL");
        history.push({
          pathname: "/adminpanel",
          userInfo: userInfos,
          auth: true,
          products: products
        });
      } else if (this.state.userType == "customer") {
        console.log("CUSTOMERPANEL");
        console.log(products);
        history.push({
          pathname: "/home",
          userInfo: userInfos,
          auth: true,
          products: products
        });
      } else if (this.state.userType == "seller") {
        console.log("SELLERPANEL");
        history.push({
          pathname: "/sellerpanel",
          userInfo: userInfos,
          auth: true,
          products: products
        });
      }
    } else {
      console.log("wrong email or password");
      this.setState({ isLoginFailError: true });
    }
  };
  render() {
    // Handle visibility of NavigationBar components
    return (
      <div>
        <NavigationBar auth={false} />
        <div className="Login">
          <form onSubmit={this.handleSubmit} className="Login-view">
            <FormGroup controlId="email" bsSize="large">
              <FormLabel className="Label">Email</FormLabel>
              <FormControl
                className="Input"
                autoFocus
                type="email"
                value={this.state.email}
                onChange={this.handleChange}
              />
              <FormText className="text-muted">
                We'll never share your email with anyone else.
              </FormText>
            </FormGroup>
            <FormGroup controlId="password" bsSize="large">
              <FormLabel className="Label"> Password</FormLabel>
              <FormControl
                className="Input"
                value={this.state.password}
                onChange={this.handleChange}
                type="password"
              />
            </FormGroup>
            <Button
              variant="dark"
              type="submit"
              disabled={!this.validateForm()}
              block
            >
              Sign in
            </Button>
            <div className="register-query">
              <div
                style={{
                  display: this.state.isLoginFailError,
                  color: "red",
                  marginTop: "3px"
                }}
              >
                {this.errors.loginFailError}
              </div>
              <FormText className="text-muted">Not registered yet?</FormText>
              <LinkButton to="/register" onClick={() => true} variant="link">
                Sign up
              </LinkButton>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

{
  /* <Button
className="Button"
block
bsSize="large"
disabled={!this.validateForm()}
type="submit"
>
Login
</Button> */
}
