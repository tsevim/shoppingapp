import React, { Component } from "react";
import "../assets/Home/Home.css";
import { FormControl, Button } from "react-bootstrap";

export default class CartProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      qty: this.props.product.qty
    };
  }
  removeProduct = e => {
    const product = e.target.value;
    this.props.removeProduct(product);
  };

  qtyUpdate = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  };

  priceUpdate = event => {
    const qty = event.target.value;
    this.props.qtyChange(this.props.product, qty);
  };

  render() {
    var { product } = this.props;
    // Change the option down bar as the text pane and add update button
    return (
      <tr>
        <td>
          <img variant="top" className="cart-img" src={product.imageUrl} />
        </td>
        <td>
          <div className="cart-brand">
            <strong>{product.name}</strong>
          </div>
        </td>
        <td>
          <div className="cart-info">{product.information}</div>
        </td>
        <td>
          <FormControl
            className="qty-button"
            id="qty"
            value={this.state.qty}
            onChange={this.qtyUpdate}
          />
          <Button
            value={this.state.qty}
            className="qty-update-button"
            onClick={this.priceUpdate}
          >
            Update
          </Button>
        </td>
        <td>
          <div className="cart-price">${product.qty * product.price}</div>
          <Button
            value={JSON.stringify(product)}
            variant="link"
            onClick={this.removeProduct}
            className="cart-remove-button"
          >
            Remove
          </Button>
        </td>
      </tr>
    );
  }
}
