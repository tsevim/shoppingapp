import React, { Component } from "react";
import "../assets/ShoppingCart/ShoppingCart.css";
import NavigationBar from "../NavBar/NavigationBar";
import { Col, Container, Row, Table, Card, Button } from "react-bootstrap";
import CartProduct from "./CartProduct";
import { Link } from "react-router-dom";

export default class ShoppingCart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      itemNumber: 0,
      totalCost: 0
    };
  }

  async componentDidMount() {
    var userInfo = this.props.location.userInfo;
    const fetch_url = "/api/customer/" + userInfo.userId + "/card";

    const response = await fetch(fetch_url, {
      headers: {
        "session-id": userInfo.sessionId,
        "Content-Type": "application/json"
      }
    });
    const body = await response.json();
    console.log(body);
    this.setState({ products: body });
    var total = 0;
    var totalItemNumber = 0;
    for (var i in this.state.products) {
      total +=
        parseInt(this.state.products[i].qty, 10) * this.state.products[i].price;
      totalItemNumber += parseInt(this.state.products[i].qty, 10);
    }
    this.setState({
      products: body,
      itemNumber: totalItemNumber,
      totalCost: total
    });
  }

  handleSubmit = e => {
    var userInfo = this.props.location.userInfo;
    const fetch_url = "/api/customer/buy/" + userInfo.userId;
    fetch(fetch_url, {
      method: "GET",
      headers: {
        "session-id": userInfo.sessionId,
        "Content-Type": "application/json"
      }
    });
  };

  removeProduct = product => {
    var productObj = JSON.parse(product);
    var productId = productObj.id;
    var cat = productObj.category;
    var updatedProducts = this.state.products;
    updatedProducts = updatedProducts.filter(
      product => product.id != productId
    );
    this.setState({
      products: updatedProducts,
      itemNumber: this.state.itemNumber - productObj.qty,
      totalCost: this.state.totalCost - productObj.qty * productObj.price
    });
    var userInfo = this.props.location.userInfo;
    const fetch_url = "/api/customer/" + userInfo.userId + "/card";
    fetch(fetch_url, {
      method: "DELETE",
      headers: {
        "session-id": userInfo.sessionId,
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        id: productId,
        category: cat
      })
    });
  };

  qtyChange = (product, newQty) => {
    var updatedProducts = this.state.products;
    var prevQty = product.qty;
    if (newQty.length === 0) {
      var newQty = 0;
    } else {
      var newQty = parseInt(newQty);
    }
    var pid;
    var cat;
    for (var i in updatedProducts) {
      if (updatedProducts[i].id == product.id) {
        updatedProducts[i].qty = newQty;
        pid = updatedProducts[i].id;

        cat = updatedProducts[i].category;
        break;
      }
    }
    this.setState({
      products: updatedProducts,
      itemNumber: this.state.itemNumber + newQty - prevQty,
      totalCost: this.state.totalCost + (newQty - prevQty) * product.price
    });

    var userInfo = this.props.location.userInfo;
    const fetch_url = "/api/customer/" + userInfo.userId + "/card";
    fetch(fetch_url, {
      method: "PUT",
      headers: {
        "session-id": userInfo.sessionId,
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        id: pid,
        qty: newQty,
        category: cat
      })
    });
  };

  render() {
    var toCheckout = {
      pathname: "/checkout",
      userInfo: this.props.location.userInfo,
      auth: this.props.location.auth,
      products: this.props.location.products
    };
    if (this.state.products.length == 0) {
      return (
        <div>
          <NavigationBar
            mode="customer"
            auth={this.props.location.auth}
            userInfo={this.props.location.userInfo}
            history={this.props.history}
            products={this.props.location.products}
          />
          <Container>
            <Row>
              <Col className="table-column">
                <Card className="empty-cart">
                  <Card.Header>
                    <Card.Title>
                      {" "}
                      You don't have any items in the cart.{" "}
                    </Card.Title>
                  </Card.Header>
                  <Card.Body>
                    {/* <LinkButton className="start-shopping" to="/home">
                      Start Shopping
                    </LinkButton> */}
                    {/* 
                    <LinkButton to="/register" onClick={() => true} variant="link">
Sign up
</LinkButton> */}
                  </Card.Body>
                </Card>
                <br />
              </Col>
            </Row>
          </Container>
        </div>
      );
    }
    return (
      <div>
        <NavigationBar
          mode="customer"
          auth={this.props.location.auth}
          userInfo={this.props.location.userInfo}
          history={this.props.history}
        />
        <Container>
          <Row>
            <Col className="table-column">
              <Table className="table">
                <thead>
                  <tr>
                    <th>Product</th>
                    <th />
                    <th>Information</th>
                    <th>Qty</th>
                    <th>Price</th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.products.map(product => (
                    <CartProduct
                      key={product.id}
                      product={product}
                      removeProduct={this.removeProduct}
                      qtyChange={this.qtyChange}
                    />
                  ))}
                </tbody>
              </Table>
            </Col>
            <Col>
              <Card className="cart-checkout">
                <Card.Header>
                  <Card.Title>
                    <strong>${this.state.totalCost}</strong> for{" "}
                    <strong>{this.state.itemNumber}</strong> item(s)
                  </Card.Title>
                </Card.Header>
                <Card.Body>
                  <Link to={toCheckout}>
                    <Button block variant="dark">
                      {" "}
                      Go to checkout{" "}
                    </Button>
                  </Link>
                </Card.Body>
              </Card>
              <br />
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}
