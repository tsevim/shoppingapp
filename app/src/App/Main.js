import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Login from "../LoginPage/Login";
import Register from "../LoginPage/Register";
import CustomerHome from "../HomePage/CustomerHome";
import AdminHome from "../HomePage/AdminHome";
import SellerHome from "../HomePage/SellerHome";
import ShoppingCart from "../ShoppingCart/ShoppingCart";
import Account from "../Account/Account";
import ProductDetail from "../Product/ProductDetail";
import SellerProductDetail from "../Product/SellerProduct/ProductDetail/SellerProductDetail";
import Sellers from "../Users/Sellers";
import Customers from "../Users/Customers";
import CustomerDetail from "../Users/CustomerDetail";
import SellerDetail from "../Users/SellerDetail";
import Statistics from "../Statistics/Statistics";
import SellerStatistics from "../Statistics/SellerStatistics";
import AddProduct from "../Product/SellerProduct/AddProduct/AddProduct";
import AddClothingProduct from "../Product/SellerProduct/AddProduct/AddClothingProduct";
import AddTechnologyProduct from "../Product/SellerProduct/AddProduct/AddTechnologyProduct";
import AddInstrumentProduct from "../Product/SellerProduct/AddProduct/AddInstrumentProduct";
import Checkout from "../Checkout/Checkout";

// The Main component renders one of the three provided
// Routes (provided that one matches). Both the /roster
// and /schedule routes will match any pathname that starts
// with /roster or /schedule. The / route will only match
// when the pathname is exactly the string "/"

const Main = () => (
  <main>
    <Router>
      <Switch>
        <Route exact path="/" component={Login} />
        <Route path="/register" component={Register} />
        <Route path="/home" component={CustomerHome} />
        <Route path="/adminpanel" component={AdminHome} />
        <Route path="/sellerpanel" component={SellerHome} />
        <Route path="/customers/:customerId" component={CustomerDetail} />
        <Route path="/customers" component={Customers} />
        <Route path="/sellers/:sellerId" component={SellerDetail} />
        <Route path="/sellers" component={Sellers} />
        <Route path="/statistics" component={Statistics} />
        <Route path="/cart" component={ShoppingCart} />
        <Route path="/account" component={Account} />
        <Route path="/products/:productId" component={ProductDetail} />
        <Route
          path="/sellerproducts/:productId"
          component={SellerProductDetail}
        />
        <Route path="/addproduct/instrument" component={AddInstrumentProduct} />
        <Route path="/addproduct/technology" component={AddTechnologyProduct} />
        <Route path="/addproduct/clothing" component={AddClothingProduct} />
        <Route path="/addproduct" component={AddProduct} />
        <Route path="/seller-statistics" component={SellerStatistics} />
        <Route path="/checkout" component={Checkout} />
      </Switch>
    </Router>
  </main>
);

export default Main;
