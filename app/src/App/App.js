import React, { Component } from "react";
import Main from "./Main";
import "../assets/App/App.css";

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imgUrl: require("../assets/images/white.png")
    };
  }

  render() {
    return (
      <div
        className="App"
        style={{
          backgroundImage: "url(" + this.state.imgUrl + ")"
        }}
      >
        <Main />
      </div>
    );
  }
}
