import React, { Component } from "react";
import {
  Card,
  Row,
  Col,
  Container,
  Button,
  FormControl
} from "react-bootstrap";
import "../assets/Product/ProductDetailed.css";
import NavigationBar from "../NavBar/NavigationBar";
import SearchBar from "../SearchBar/SearchBar";
export default class ProductDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      qty: 1,
      isNotLoggedInError: "none",
      isItemAddedSuccess: "none"
    };

    this.successes = {
      itemAddedSuccess: "The item is added to cart!"
    };

    this.errors = {
      notLoggedInError: "You need to be login to add to cart!"
    };
  }

  qtyChange = event => {
    const qty = event.target.value;
    this.setState({
      [event.target.id]: qty
    });
  };

  async addToCart(product) {
    var userInfo = this.props.location.userInfo;
    console.log(userInfo);
    if (userInfo != null) {
      // authorized user
      product.qty = this.state.qty;
      const fetch_url = "/api/customer/" + userInfo.userId + "/card";
      var isProductAdded = await fetch(fetch_url, {
        method: "PUT",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          "session-id": userInfo.sessionId
        },
        body: JSON.stringify(product)
      }).then(function(response) {
        if (!response.ok) {
          alert("Problem occurred.");
          return false;
        } else {
          return true;
        }
      });
      if (isProductAdded) {
        this.setState({ isItemAddedSuccess: true });
        const history = this.props.history;

        var toHome = {
          pathname: "/cart",
          userInfo: this.props.location.userInfo,
          auth: this.props.location.auth
        };

        setTimeout(function() {
          console.log(",", history);
          history.push(toHome);
        }, 1200);
      }
    } else {
      // unauthorized user
      const { history } = this.props;
      this.setState({ isNotLoggedInError: true });
      setTimeout(function() {
        history.push({ pathname: "/" });
      }, 2000);
    }
  }

  render() {
    var product = this.props.location.product;
    return (
      <div>
        <NavigationBar
          mode="customer"
          auth={this.props.location.auth}
          userInfo={this.props.location.userInfo}
          history={this.props.history}
          products={this.props.location.products}
        />
        <Container>
          <Row>
            <Col>
              <Card className="product-detailed">
                <Card.Img className="img-detailed" src={product.imageUrl} />
              </Card>
            </Col>
            <Col>
              <div className="product-info-detailed">
                <Card>
                  <Card.Body>
                    <Card.Title className="product-title">
                      <strong>{product.name}</strong>
                    </Card.Title>
                    <Card.Text className="product-info">
                      {product.information}
                    </Card.Text>
                    <Card.Text className="product-price">
                      <strong>${product.price}</strong>
                    </Card.Text>
                    <FormControl
                      as="select"
                      className="qty-button"
                      id="qty"
                      onChange={this.qtyChange}
                    >
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                    </FormControl>
                    <Button
                      variant="dark"
                      type="submit"
                      className="cart-button"
                      onClick={e => this.addToCart(product)}
                    >
                      Add to cart
                    </Button>{" "}
                    <div
                      style={{
                        display: this.state.isNotLoggedInError,
                        color: "red",
                        marginTop: "8px"
                      }}
                    >
                      {this.errors.notLoggedInError}
                    </div>
                    <div
                      style={{
                        display: this.state.isItemAddedSuccess,
                        color: "green",
                        marginTop: "8px"
                      }}
                    >
                      <b>{this.successes.itemAddedSuccess}</b>
                    </div>
                  </Card.Body>
                </Card>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}
