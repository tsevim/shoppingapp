import React, { Component } from "react";
import { Button, FormControl, Form, Container } from "react-bootstrap";
import "../../assets/AdminPanel/adminpanel.css";
import { slide as Menu } from "react-burger-menu";
import { Link } from "react-router-dom";
export default class SellerLeftBar extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="home">
        <Menu>
          <Link
            id="home"
            className="menu-item"
            to={{
              pathname: "/addproduct", //sellerid,
              userInfo: this.props.userInfo,
              auth: this.props.auth,
              products: this.props.products
            }}
          >
            Add Product
          </Link>
          <Link
            id="home"
            className="menu-item"
            to={{
              pathname: "/seller-statistics", // change
              userInfo: this.props.userInfo,
              auth: this.props.auth,
              products: this.props.products
            }}
          >
            Statistics
          </Link>
        </Menu>
      </div>
    );
  }
}
