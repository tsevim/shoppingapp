import React, { Component } from "react";
import {
  Card,
  Row,
  Col,
  Container,
  Button,
  FormControl,
  FormGroup,
  FormLabel
} from "react-bootstrap";
import "../../../assets/Product/ProductDetailed.css";

export default class InstrumentProductDetail extends Component {
  constructor(props) {
    super(props);
    const product = this.props.product;
    this.state = {
      displayedImageUrl: product.imageUrl,
      showInitialImageUrl: product.showInitialImageUrl,
      brand: product.brand,
      category: product.category,
      color: product.color,
      imageUrl: product.imageUrl,
      information: product.information,
      name: product.name,
      price: product.price,
      isElectro: product.isElectro,
      type: product.type,
      woodType: product.woodType,
      isProductProcessedSuccess: "none",
      isPriceNotNumberError: "none",
      isProductDeleted: "none"
    };

    this.successes = {
      productProcessedSuccess:
        this.props.isAdd == true
          ? "Product is added successfully!"
          : "Product is updated successfully!"
    };

    this.errors = {
      priceNotNumberError: "Please enter a number!",
      productDeletedError: "Product is deleted successfully!"
    };
  }

  handleChange = event => {
    if (event.target.id == "imageUrl")
      this.setState({ showInitialImageUrl: true });
    this.setState({
      [event.target.id]: event.target.value
    });
  };

  deleteProduct = event => {
    var productInfo = {
      category: "instrument",
      id: this.props.product.id,
      sellerId: this.props.userInfo.userId
    };

    fetch("/api/product", {
      method: "DELETE",
      headers: {
        "session-id": this.props.userInfo.sessionId,
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(productInfo)
    });
    const history = this.props.history;
    console.log("...", history);
    this.setState({ isProductDeleted: true });
    var toHome = {
      pathname: "/sellerpanel",
      userInfo: this.props.userInfo,
      auth: this.props.auth
    };
    setTimeout(function() {
      console.log(",", history);
      history.push(toHome);
    }, 1500);
  };

  processNewProduct = event => {
    var processType = event.target.value;
    if (isNaN(this.state.price)) {
      this.setState({
        isPriceNotNumberError: true,
        isProductProcessedSuccess: "none"
      });
    } else {
      var db_img_url = this.state.imageUrl;
      if (this.state.imageUrl.trim() == "")
        db_img_url = this.state.displayedImageUrl;
      this.setState({
        displayedImageUrl: this.state.imageUrl,
        isPriceNotNumberError: "none"
      });
      // add product
      console.log("add instrument product");
      var newProductInfo = {
        sellerId: this.props.userInfo.userId,
        color: this.state.color,
        price: this.state.price,
        imageUrl: db_img_url,
        information: this.state.information,
        name: this.state.name,
        category: "instrument",
        brand: this.state.brand,
        stock: 1, // default
        type: this.state.type,
        woodType: this.state.woodType
      };

      if (processType == "update") {
        newProductInfo.id = this.props.product.id;
      }
      console.log(newProductInfo);

      fetch("/api/product", {
        method: "POST",
        headers: {
          "session-id": this.props.userInfo.sessionId,
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify(newProductInfo)
      });

      const history = this.props.history;
      this.setState({ isProductProcessedSuccess: true });

      var toHome = {
        pathname: "/sellerpanel",
        userInfo: this.props.userInfo,
        auth: this.props.auth
      };
      setTimeout(function() {
        console.log(",", history);
        history.push(toHome);
      }, 1500);
    }
  };

  validateForm() {
    var isValid = false;
    try {
      isValid =
        this.state.name.length > 0 &&
        this.state.information.length > 0 &&
        this.state.brand.length > 0 &&
        this.state.color.length > 0 &&
        this.state.price.length > 0 &&
        this.state.type.length > 0 &&
        this.state.woodType.length > 0;
    } catch (err) {}
    return isValid;
  }

  render() {
    console.log(this.props.product);
    return (
      <div>
        <Container>
          <Row>
            <Col>
              <Card className="product-detailed">
                <Card.Img
                  className="img-detailed"
                  src={this.state.displayedImageUrl}
                />
              </Card>
            </Col>
            <Col>
              <div className="product-info-detailed">
                <FormGroup controlId="name" bsSize="large">
                  <FormLabel className="Label">Name</FormLabel>
                  <FormControl
                    className="Input"
                    value={this.state.name}
                    onChange={this.handleChange}
                    type="name"
                  />
                </FormGroup>
                <FormGroup controlId="information" bsSize="large">
                  <FormLabel className="Label">Information</FormLabel>
                  <FormControl
                    as="textarea"
                    className="Input"
                    value={this.state.information}
                    onChange={this.handleChange}
                    type="name"
                  />
                </FormGroup>
                <FormGroup controlId="price" bsSize="large">
                  <FormLabel className="Label">Price ($)</FormLabel>
                  <FormControl
                    className="Input"
                    value={this.state.price}
                    onChange={this.handleChange}
                    type="name"
                  />
                </FormGroup>
                <div
                  style={{
                    display: this.state.isPriceNotNumberError,
                    color: "red",
                    marginTop: "-8px"
                  }}
                >
                  {this.errors.priceNotNumberError}
                </div>
                <FormGroup controlId="category" bsSize="large">
                  <FormLabel className="Label">Category</FormLabel>
                  <FormControl
                    className="Input"
                    onChange={this.handleChange}
                    defaultValue="Instrument"
                    disabled
                    type="name"
                  />
                </FormGroup>
                <FormGroup controlId="brand" bsSize="large">
                  <FormLabel className="Label">Brand</FormLabel>
                  <FormControl
                    className="Input"
                    value={this.state.brand}
                    onChange={this.handleChange}
                    type="name"
                  />
                </FormGroup>
                <FormGroup controlId="color" bsSize="large">
                  <FormLabel className="Label">Color</FormLabel>
                  <FormControl
                    className="Input"
                    value={this.state.color}
                    onChange={this.handleChange}
                    type="name"
                  />
                </FormGroup>
                <FormGroup controlId="isElectro" bsSize="large">
                  <FormLabel className="Label">Is Electric</FormLabel>
                  <FormControl
                    as="select"
                    //className="qty-button"
                    onChange={this.handleChange} // test if updates the state
                  >
                    <option value="true">Yes</option>
                    <option value="false">No</option>
                  </FormControl>
                </FormGroup>
                <FormGroup controlId="type" bsSize="large">
                  <FormLabel className="Label">Instrument Type</FormLabel>
                  <FormControl
                    className="Input"
                    value={this.state.type}
                    onChange={this.handleChange}
                    type="name"
                  />
                </FormGroup>
                <FormGroup controlId="woodType" bsSize="large">
                  <FormLabel className="Label">Wood Type</FormLabel>
                  <FormControl
                    className="Input"
                    value={this.state.woodType}
                    onChange={this.handleChange}
                    type="name"
                  />
                </FormGroup>
                <FormGroup controlId="imageUrl" bsSize="large">
                  <FormLabel className="Label">Image Url</FormLabel>
                  <FormControl
                    as="textarea"
                    className="Input"
                    value={
                      this.state.showInitialImageUrl == false
                        ? ""
                        : this.state.imageUrl
                    }
                    onChange={this.handleChange}
                    type="name"
                  />
                </FormGroup>
                <Button
                  variant="dark"
                  type="submit"
                  className="cart-button"
                  value={this.props.isAdd == true ? "add" : "update"}
                  onClick={this.processNewProduct}
                  disabled={!this.validateForm()}
                >
                  {this.props.isAdd == true ? "Add Product" : "Update"}
                </Button>{" "}
                <div
                  style={{
                    display: this.props.isAdd == true ? "none" : true,
                    marginLeft: "10px"
                  }}
                >
                  <Button
                    variant="danger"
                    type="submit"
                    className="cart-button"
                    onClick={this.deleteProduct}
                    disabled={!this.validateForm()}
                  >
                    Delete
                  </Button>{" "}
                </div>
                <div
                  style={{
                    display: this.state.isProductProcessedSuccess,
                    color: "green",
                    marginTop: "10px"
                  }}
                >
                  <b>{this.successes.productProcessedSuccess}</b>
                </div>
                <div
                  style={{
                    display: this.state.isProductDeleted,
                    color: "red",
                    marginTop: "10px"
                  }}
                >
                  <b>{this.errors.productDeletedError}</b>
                </div>
              </div>
            </Col>
          </Row>
        </Container>
        ;
      </div>
    );
  }
}
