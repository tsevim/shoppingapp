import React, { Component } from "react";
import {
  Card,
  Row,
  Col,
  Container,
  Button,
  FormControl,
  FormGroup,
  FormLabel
} from "react-bootstrap";
import "../../../assets/Product/ProductDetailed.css";
import NavigationBar from "../../../NavBar/NavigationBar";

import TechnologyProductDetail from "./TechnologyProductDetail";
import InstrumentProductDetail from "./InstrumentProductDetail";
import ClothingProductDetail from "./ClothingProductDetail";

export default class SellerProductDetail extends Component {
  constructor(props) {
    super(props);
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  };

  updateProduct(product) {
    console.log(product);
    // update product of the seller in the database
  }

  render() {
    console.log(this.props);
    var product = this.props.location.product;
    if (product.category == "technology") {
      return (
        <div>
          <NavigationBar
            mode="seller"
            auth={this.props.location.auth}
            userInfo={this.props.location.userInfo}
            history={this.props.history}
            products={this.props.location.products}
          />
          <TechnologyProductDetail
            product={this.props.location.product}
            userInfo={this.props.location.userInfo}
            auth={this.props.location.auth}
            history={this.props.history}
          />
        </div>
      );
    } else if (product.category == "clothing") {
      return (
        <div>
          <NavigationBar
            mode="seller"
            auth={this.props.location.auth}
            userInfo={this.props.location.userInfo}
            history={this.props.history}
            products={this.props.location.products}
          />
          <ClothingProductDetail
            product={this.props.location.product}
            userInfo={this.props.location.userInfo}
            auth={this.props.location.auth}
            history={this.props.history}
          />
        </div>
      );
    } else if (product.category == "instrument") {
      return (
        <div>
          <NavigationBar
            mode="seller"
            auth={this.props.location.auth}
            userInfo={this.props.location.userInfo}
            history={this.props.history}
            products={this.props.location.products}
          />
          <InstrumentProductDetail
            product={this.props.location.product}
            userInfo={this.props.location.userInfo}
            auth={this.props.location.auth}
            history={this.props.history}
          />
        </div>
      );
    }
  }
}
