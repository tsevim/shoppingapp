import React, { Component } from "react";
import {
  Button,
  FormControl,
  Form,
  Container,
  Card,
  Col,
  Row
} from "react-bootstrap";
import "../../../Statistics/statistics.css";
import NavigationBar from "../../../NavBar/NavigationBar";
import { Link } from "react-router-dom";
import SellerLeftBar from "../SellerLeftBar";

export default class AddProduct extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    var defaultImageUrl =
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR1ilCBiZSnQJVXOcHFsH4O73hTLLYdAyKmqtF2Sh5GSazD7By2nw";
    console.log(this.props);
    return (
      <div className="home">
        <NavigationBar
          mode="seller"
          history={this.props.history}
          auth={this.props.location.auth}
          userInfo={this.props.location.userInfo}
          products={this.props.location.products}
        />
        <SellerLeftBar
          userInfo={this.props.location.userInfo}
          auth={this.props.location.auth}
          products={this.props.location.products}
        />
        <Container>
          <Row>
            <Col>
              <Card
                className="statistics-title"
                border="dark"
                style={{ width: "15rem" }}
              >
                <Link
                  to={{
                    pathname: "/addproduct/technology",
                    userInfo: this.props.location.userInfo,
                    auth: this.props.location.auth,
                    products: this.props.location.products,
                    defaultImageUrl: defaultImageUrl
                  }}
                >
                  <Card.Img
                    variant="top"
                    src="https://www.teknoburada.net/wp-content/uploads/2017/12/Abra-A5-v11_01_large.jpg"
                  />
                </Link>
                <Card.Body>
                  <Card.Title>Technology</Card.Title>
                  <Card.Text />
                </Card.Body>
              </Card>
            </Col>
            <Col>
              <Card
                className="statistics-title"
                border="dark"
                style={{ width: "15rem" }}
              >
                <Link
                  to={{
                    pathname: "/addproduct/clothing",
                    userInfo: this.props.location.userInfo,
                    auth: this.props.location.auth,
                    products: this.props.location.products,
                    defaultImageUrl: defaultImageUrl
                  }}
                >
                  <Card.Img
                    variant="top"
                    src="https://sc01.alicdn.com/kf/HTB118IcSFXXXXcRXpXXq6xXFXXXI/Bulk-stock-lowest-price-no-moq-polyester.jpg"
                  />
                </Link>
                <Card.Body>
                  <Card.Title>Clothing</Card.Title>
                  <Card.Text />
                </Card.Body>
              </Card>
            </Col>
            <Col>
              <Card
                className="statistics-title"
                border="dark"
                style={{ width: "15rem" }}
              >
                <Link
                  to={{
                    pathname: "/addproduct/instrument",
                    userInfo: this.props.location.userInfo,
                    auth: this.props.location.auth,
                    products: this.props.location.products,
                    defaultImageUrl: defaultImageUrl
                  }}
                >
                  <Card.Img
                    variant="top"
                    src="https://www.long-mcquade.com/files/16630/lg_godin-session-rosewood-blackburst-1.jpg"
                  />
                </Link>
                <Card.Body>
                  <Card.Title>Instrument</Card.Title>
                  <Card.Text />
                </Card.Body>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}
