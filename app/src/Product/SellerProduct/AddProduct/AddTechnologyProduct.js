import React, { Component } from "react";
import {
  Card,
  Row,
  Col,
  Container,
  Button,
  FormControl,
  FormGroup,
  FormLabel
} from "react-bootstrap";
import "../../../assets/Product/ProductDetailed.css";
import TechnologyProductDetail from "../ProductDetail/TechnologyProductDetail";
import NavigationBar from "../../../NavBar/NavigationBar";
import SellerLeftBar from "../SellerLeftBar";

export default class AddTechnologyProduct extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <NavigationBar
          mode="seller"
          history={this.props.history}
          auth={this.props.location.auth}
          userInfo={this.props.location.userInfo}
          products={this.props.location.products}
        />
        <SellerLeftBar
          userInfo={this.props.location.userInfo}
          auth={this.props.location.auth}
          products={this.props.location.products}
        />
        <TechnologyProductDetail
          product={{
            imageUrl: this.props.location.defaultImageUrl,
            showInitialImageUrl: false
          }}
          userInfo={this.props.location.userInfo}
          isAdd={true}
          history={this.props.history}
          auth={this.props.location.auth}
        />
      </div>
    );
  }
}
