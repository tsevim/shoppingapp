import React from "react";
import ProductView from "./ProductView";
import SellerProductView from "./SellerProduct/SellerProductView";
import { Row, Card } from "react-bootstrap";
import "../assets/Product/Product.css";

export default class ProductList extends React.Component {
  render() {
    if (this.props.mode == "detailed") {
      return (
        <div>
          <Row style={{ margin: "0 auto" }}>
            {this.props.products.map(product => (
              <ProductView
                key={product.id}
                product={product}
                userInfo={this.props.userInfo}
                auth={this.props.auth}
                products={this.props.products}
              />
            ))}
          </Row>
        </div>
      );
    } else if (this.props.mode == "seller") {
      return (
        <div>
          <Row style={{ margin: "0 auto" }}>
            {this.props.products.map(product => (
              <SellerProductView
                key={product.id}
                product={product}
                userInfo={this.props.userInfo}
                auth={this.props.auth}
                products={this.props.products}
              />
            ))}
          </Row>
        </div>
      );
    } else if (this.props.mode == "preview") {
      return (
        <div>
          <Row style={{ margin: "0 auto" }}>
            {this.props.products.map(product => (
              <Card style={{ width: "14rem" }} className="product">
                <div className="img-wrapper">
                  <Card.Img
                    variant="top"
                    className="img"
                    src={product.imageUrl}
                  />
                </div>
                <Card.Body>
                  <Card.Title className="ellipsis"> {product.name}</Card.Title>
                  <Card.Text className="ellipsis">
                    {product.information}
                  </Card.Text>
                  <Card.Text className="price"> ${product.price}</Card.Text>
                </Card.Body>
              </Card>
            ))}
          </Row>
        </div>
      );
    }
  }
}
