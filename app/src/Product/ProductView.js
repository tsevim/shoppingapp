import React, { Component } from "react";
import { Card } from "react-bootstrap";
import { Link } from "react-router-dom";

import "../assets/Product/Product.css";

export default class ProductView extends Component {
  constructor(props) {
    super(props);
    // this.state = {
    //   imgUrl: require(this.props.product.picture)
    // };
  }

  render() {
    var { product } = this.props;
    var toChild = {
      pathname: "products/" + product.id,
      product: product,
      userInfo: this.props.userInfo,
      auth: this.props.auth,
      products: this.props.products
    }; // product_category/product_id should be the routing url after categories are added
    return (
      <Card style={{ width: "18rem" }} className="product">
        <Link to={toChild}>
          <div className="img-wrapper">
            <Card.Img variant="top" className="img" src={product.imageUrl} />
          </div>
        </Link>
        <Card.Body>
          <Card.Title className="ellipsis"> {product.name}</Card.Title>
          <Card.Text className="ellipsis">{product.information}</Card.Text>
          <Card.Text className="price"> ${product.price}</Card.Text>
        </Card.Body>
      </Card>
    );
  }
}

// <div className="product-img-wrapper">
//           <a href="#">
//             <img
//               alt={product.name}
//               className="img-responsive product-img"
//               src={product.picture}
//             />
//           </a>
//         </div>
//         <h4 className="ellipsis" title={product.name}>
//           <a href="#">{product.name}</a>
//         </h4>
//         <h5 className="ellipsis product-brand-name" title={product.brand_name}>
//           {`by ${product.brand_name}`}
//         </h5>
//         <div className="pull-right h4 product-price">{`${product.price}$`}</div>
//       </Card>
