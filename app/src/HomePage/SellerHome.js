import React, { Component } from "react";
import {
  Button,
  FormControl,
  Form,
  Container,
  NavDropdown
} from "react-bootstrap";
import ProductList from "../Product/ProductList";
import "../assets/Home/Home.css";
import NavigationBar from "../NavBar/NavigationBar";
import SearchBar from "../SearchBar/SearchBar";
import { slide as Menu } from "react-burger-menu";
import { Link } from "react-router-dom";
import SellerLeftBar from "../Product/SellerProduct/SellerLeftBar";

export default class SellerHome extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      userInfo: this.props.location.userInfo,
      sellerInfo: "",
      sellerProducts: [],
      allSellerProducts: [],
      isOpen: false,
      filter: "Brand",
      searchWord: ""
    };
    console.log("const");
  }

  async componentDidMount() {
    var response = await fetch("/api/products");
    const productBody = await response.json();
    this.setState({ products: productBody });

    var response = await fetch("/api/seller/" + this.state.userInfo.userId, {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "session-id": this.state.userInfo.sessionId,
        aid: this.props.location.userInfo.userId // works with sessionId ?
      }
    });
    var sellersBody = await response.json();

    var products = this.state.products;
    var sellersProductIDs = sellersBody.productIDs;
    var sellerProducts = [];
    for (var i in products) {
      var productId = products[i].id;
      for (var id in sellersProductIDs) {
        if (productId == id) {
          sellerProducts.push(products[i]);
          break;
        }
      }
    }

    this.setState({
      products: productBody,
      sellerInfo: sellersBody,
      sellerProducts: sellerProducts,
      allSellerProducts: sellerProducts
    });
  }

  search = async event => {
    this.props.location.retrieveAllProducts = "false";
    var query = "?";
    var filter = this.state.filter;
    query += filter.toLowerCase() + "=" + this.state.searchWord;
    console.log("query", query);
    const response = await fetch("/api/products" + query);
    const body = await response.json();
    console.log("query result:", body);
    this.setState({ sellerProducts: body });
  };

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  };

  updateFilter = event => {
    this.setState({ filter: event.target.id });
  };

  handleOpen = () => {
    this.setState({ isOpen: true });
  };

  render() {
    console.log("sellerHome", this.props);
    return (
      <div className="home">
        <NavigationBar
          mode="seller"
          auth={this.props.location.auth}
          userInfo={this.props.location.userInfo}
          history={this.props.history}
          products={this.state.allSellerProducts}
        />
        <SellerLeftBar
          userInfo={this.props.location.userInfo}
          auth={this.props.location.auth}
          products={this.state.allSellerProducts}
        />
        <div>
          <Form inline>
            <div className="search">
              <FormControl
                type="text"
                placeholder="Search"
                className="search-bar"
                id="searchWord"
                onChange={this.handleChange}
              />
              <Button
                variant="secondary"
                onClick={this.search}
                className="search-button"
              >
                Search
              </Button>
              <NavDropdown
                onClick={this.handleOpen}
                open={this.state.isOpen}
                title={this.state.filter}
              >
                <NavDropdown.Item
                  id="Brand"
                  onClick={e => this.updateFilter(e)}
                >
                  Brand
                </NavDropdown.Item>
                <NavDropdown.Item id="Name" onClick={e => this.updateFilter(e)}>
                  Name
                </NavDropdown.Item>
                <NavDropdown.Item
                  id="Category"
                  onClick={e => this.updateFilter(e)}
                >
                  Category
                </NavDropdown.Item>
              </NavDropdown>
            </div>
          </Form>
          <div>
            <Container>
              <ProductList
                mode="seller"
                products={
                  this.props.location.retrieveAllProducts == "true"
                    ? this.state.allSellerProducts
                    : this.state.sellerProducts
                }
                auth={this.props.location.auth}
                userInfo={this.props.location.userInfo}
              />
            </Container>
          </div>
        </div>
      </div>
    );
  }
}
