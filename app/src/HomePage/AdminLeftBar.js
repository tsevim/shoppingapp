import React, { Component } from "react";
import { Button, FormControl, Form, Container } from "react-bootstrap";
import "../assets/AdminPanel/adminpanel.css";
import { slide as Menu } from "react-burger-menu";
import SearchBar from "../SearchBar/SearchBar";
import { Link } from "react-router-dom";
export default class AdminLeftBar extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    console.log("adminleftbarproducts", this.props.products);
    return (
      <div className="home">
        <Menu>
          <Link
            id="home"
            className="menu-item"
            to={{
              pathname: "/sellers",
              sellers: this.props.sellers,
              customers: this.props.customers,
              userInfo: this.props.userInfo,
              auth: this.props.auth,
              products: this.props.products
            }}
          >
            Sellers
          </Link>
          <Link
            id="about"
            className="menu-item"
            to={{
              pathname: "/customers",
              sellers: this.props.sellers,
              customers: this.props.customers,
              userInfo: this.props.userInfo,
              auth: this.props.auth,
              products: this.props.products
            }}
          >
            Customers
          </Link>
          <Link
            id="about"
            className="menu-item"
            to={{
              pathname: "/statistics",
              sellers: this.props.sellers,
              customers: this.props.customers,
              userInfo: this.props.userInfo,
              auth: this.props.auth,
              products: this.props.products
            }}
          >
            Statistics
          </Link>
        </Menu>
      </div>
    );
  }
}
