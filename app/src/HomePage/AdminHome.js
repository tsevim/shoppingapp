import React, { Component } from "react";
import {
  Button,
  FormControl,
  Form,
  Container,
  Card,
  Col,
  Row
} from "react-bootstrap";
import "../assets/AdminPanel/adminpanel.css";
import { slide as Menu } from "react-burger-menu";
import NavigationBar from "../NavBar/NavigationBar";
import SearchBar from "../SearchBar/SearchBar";
import { Link } from "react-router-dom";
import AdminLeftBar from "./AdminLeftBar";
import { Doughnut } from "react-chartjs-2";

export default class AdminHome extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sellers: [],
      customers: [],
      numOfProducts: 0
    };
  }

  async componentDidMount() {
    var response = await fetch("/api/sellers", {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "session-id": this.props.location.userInfo.sessionId,
        aid: this.props.location.userInfo.userId
      }
    });
    var sellersBody = await response.json();

    response = await fetch("/api/customers", {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "session-id": this.props.location.userInfo.sessionId,
        aid: this.props.location.userInfo.userId
      }
    });
    var customersBody = await response.json();
    var numOfProducts = 0;
    for (var i in sellersBody) {
      numOfProducts += parseInt(sellersBody[i].totalProduct);
    }
    console.log("numOfProducts", numOfProducts);
    this.setState({
      customers: customersBody,
      sellers: sellersBody,
      numOfProducts: numOfProducts
    });
  }

  render() {
    const data = {
      labels: ["Customers", "Sellers"],
      datasets: [
        {
          data: [this.state.customers.length, this.state.sellers.length],
          backgroundColor: ["#FF6384", "#36A2EB"],
          hoverBackgroundColor: ["#FF6384", "#36A2EB"]
        }
      ],
      width: 20,
      height: 20
    };
    console.log("customers", this.state.customers);
    console.log("sellers", this.state.sellers);
    console.log("ADMINHOME products", this.props.location.products);
    return (
      <div className="home">
        <NavigationBar
          mode="admin"
          history={this.props.history}
          auth={this.props.location.auth}
          userInfo={this.props.location.userInfo}
          products={this.props.location.products}
        />
        <AdminLeftBar
          sellers={this.state.sellers}
          customers={this.state.customers}
          auth={this.props.location.auth}
          userInfo={this.props.location.userInfo}
          products={this.props.location.products}
        />
        <Container>
          <Row>
            <Col xs={4}>
              <Card
                className="text-center"
                border="dark"
                style={{ width: "15rem" }}
              >
                <Card.Body>
                  <Card.Title>Customers</Card.Title>
                  <Card.Title>{this.state.customers.length}</Card.Title>
                </Card.Body>
              </Card>
            </Col>
            <Col xs={4}>
              <Card
                className="text-center"
                border="dark"
                style={{ width: "15rem" }}
              >
                <Card.Body>
                  <Card.Title>Sellers</Card.Title>
                  <Card.Title>{this.state.sellers.length}</Card.Title>
                </Card.Body>
              </Card>
            </Col>
            <Col xs={4}>
              <Card
                className="text-center"
                border="dark"
                style={{ width: "15rem" }}
              >
                <Card.Body>
                  <Card.Title>Products</Card.Title>
                  <Card.Title>{this.state.numOfProducts}</Card.Title>
                </Card.Body>
              </Card>
            </Col>
          </Row>
          <Row>
            <div className="donut">
              <Doughnut data={data} />{" "}
            </div>
          </Row>
        </Container>
      </div>
    );
  }
}
