import React, { Component } from "react";
import { FormGroup, FormControl, FormLabel, Button } from "react-bootstrap";
import "../assets/Sign/Sign.css";
import LinkButton from "../LoginPage/LinkButton";
import NavigationBar from "../NavBar/NavigationBar";

export default class Account extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: this.props.location.userInfo.email,
      name: this.props.location.userInfo.name,
      surname: this.props.location.userInfo.surname,
      currentPassword: "",
      newPassword: "",
      confirmPassword: "",
      accountType: this.props.location.userInfo.userType,
      isPasswordWrongError: "none",
      isPasswordMatchError: "none",
      isPasswordLengthError: "none",
      isUpdateSuccess: "none"
    };
    this.errors = {
      passwordWrongError: "Wrong password!",
      passwordMatchError: "Unmatched passwords!",
      passwordLengthError: "Passwords should be at least 8 characters!"
    };

    this.successes = {
      updateSuccess: "Update is successful!"
    };
  }
  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  };

  validateForm() {
    var isValid =
      this.state.name.length > 0 &&
      this.state.surname.length > 0 &&
      this.state.currentPassword.length > 0 &&
      this.state.newPassword.length > 0 &&
      this.state.confirmPassword.length > 0;
    return isValid;
  }

  async updateUserInfos() {
    var userInfo = this.props.location.userInfo;
    console.log(userInfo);
    console.log("UPDATED");
    const { name, surname, newPassword, accountType } = this.state;
    console.log("Testing:", userInfo.userType);
    return fetch("/api/" + userInfo.userType + "/" + userInfo.userId, {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "session-id": userInfo.sessionId,
        aid: "emp"
      },
      body: JSON.stringify({
        name: name,
        surname: surname,
        password: newPassword,
        type: accountType
      })
    });
  }

  handleSubmit = async event => {
    console.log("asdasd");
    event.preventDefault();
    var userInfo = this.props.location.userInfo;
    const { currentPassword, newPassword, confirmPassword } = this.state;
    /*const email = "taha@gmail.com";
    const password = "test1234";
    const confirmPassword = "test1234";*/
    if (currentPassword !== userInfo.password) {
      // this.props.location.userInfo.password instead of ""
      console.log("The password is not matched with the one in the database");
      this.setState({
        isPasswordWrongError: true,
        isPasswordMatchError: "none",
        isPasswordLengthError: "none",
        isUpdateSuccess: "none"
      });
      return false;
    } else if (newPassword.length < 8) {
      console.log("The password length is less than 8.");
      this.setState({
        isPasswordWrongError: "none",
        isPasswordMatchError: "none",
        isPasswordLengthError: true,
        isUpdateSuccess: "none"
      });
      return false;
    } else {
      if (newPassword !== confirmPassword) {
        this.setState({
          isPasswordWrongError: "none",
          isPasswordMatchError: true,
          isPasswordLengthError: "none",
          isUpdateSuccess: "none"
        });
        return false;
      } else {
        var responseJSON = await this.updateUserInfos();
        if (responseJSON) {
          this.setState({
            isPasswordWrongError: "none",
            isPasswordMatchError: "none",
            isPasswordLengthError: "none",
            isUpdateSuccess: true
          });
          console.log("Update is successful.");
          this.props.location.userInfo.password = newPassword; //????????????
          return true;
        } else {
          alert("update is failed for some reasons..");
          console.log("Update is failed.");
        }
      }
    }
  };

  render() {
    console.log("Accountttt");
    console.log(this.props);
    return (
      <div>
        <NavigationBar
          mode={this.props.location.mode}
          auth={this.props.location.auth}
          userInfo={this.props.location.userInfo}
          history={this.props.history}
          products={this.props.location.products}
        />
        <div className="Register">
          <form>
            <FormGroup controlId="email" bsSize="large">
              <FormLabel className="Label" disabled={true}>
                Email
              </FormLabel>
              <FormControl
                className="Input"
                autoFocus
                type="email"
                defaultValue={this.state.email}
                disabled
              />
            </FormGroup>
            <FormGroup controlId="name" bsSize="large">
              <FormLabel className="Label"> Name</FormLabel>
              <FormControl
                className="Input"
                //value={this.state.currentPassword}
                defaultValue={this.state.name}
                onChange={this.handleChange}
                type="name"
              />
            </FormGroup>
            <FormGroup controlId="surname" bsSize="large">
              <FormLabel className="Label"> Surname</FormLabel>
              <FormControl
                className="Input"
                //value={this.state.currentPassword}
                defaultValue={this.state.surname}
                onChange={this.handleChange}
                type="surname"
              />
            </FormGroup>
            <FormGroup controlId="currentPassword" bsSize="large">
              <FormLabel className="Label"> Current Password</FormLabel>
              <FormControl
                className="Input"
                value={this.state.currentPassword}
                onChange={this.handleChange}
                type="password"
              />
            </FormGroup>
            <div
              style={{
                display: this.state.isPasswordWrongError,
                color: "red",
                marginTop: "-8px"
              }}
            >
              {this.errors.passwordWrongError}
            </div>
            <FormGroup controlId="newPassword" bsSize="large">
              <FormLabel className="Label"> New Password</FormLabel>
              <FormControl
                className="Input"
                value={this.state.newPassword}
                onChange={this.handleChange}
                type="password"
              />
            </FormGroup>
            <div
              style={{
                display: this.state.isPasswordLengthError,
                color: "red",
                marginTop: "-8px"
              }}
            >
              {this.errors.passwordLengthError}
            </div>
            <FormGroup controlId="confirmPassword" bsSize="large">
              <FormLabel className="Label"> Confirm Password</FormLabel>
              <FormControl
                className="Input"
                value={this.state.confirmPassword}
                onChange={this.handleChange}
                type="password"
              />
            </FormGroup>
            <div
              style={{
                display: this.state.isPasswordMatchError,
                color: "red",
                marginTop: "-8px"
              }}
            >
              {this.errors.passwordMatchError}
            </div>
            <FormGroup controlId="accountType">
              <FormLabel>Account Type</FormLabel>
              <FormControl
                as="select"
                value={this.state.accountType}
                onChange={this.handleChange}
                disabled
              >
                <option value="customer">Customer</option>
                <option value="seller">Seller</option>
              </FormControl>
            </FormGroup>
            <Button
              onClick={this.handleSubmit}
              variant="dark"
              disabled={!this.validateForm()}
              block
            >
              Update
            </Button>
            <div
              style={{
                display: this.state.isUpdateSuccess,
                color: "green",
                marginTop: "10px"
              }}
            >
              <b>{this.successes.updateSuccess}</b>
            </div>
          </form>
        </div>
      </div>
    );
  }
}
