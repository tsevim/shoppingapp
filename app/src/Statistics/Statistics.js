import React, { Component } from "react";
import {
  Button,
  FormControl,
  Form,
  Container,
  Card,
  Col,
  Row
} from "react-bootstrap";
import "./statistics.css";
import { slide as Menu } from "react-burger-menu";
import NavigationBar from "../NavBar/NavigationBar";
import SearchBar from "../SearchBar/SearchBar";
import { Link } from "react-router-dom";
import AdminLeftBar from "../HomePage/AdminLeftBar";
import { Doughnut, Bar, Line } from "react-chartjs-2";

export default class Statistics extends Component {
  constructor(props) {
    super(props);
    this.state = {
      customers: this.props.location.customers,
      sellers: this.props.location.sellers,
      products: this.props.location.products
    };
    this.userData = {
      labels: ["Customers", "Sellers"],
      datasets: [
        {
          data: [this.state.customers.length, this.state.sellers.length],
          backgroundColor: ["#FF6384", "#36A2EB"],
          hoverBackgroundColor: ["#FF6384", "#36A2EB"]
        }
      ],
      width: 10,
      height: 10
    };

    var banned = 0;
    var normal = 0;
    for (var i in this.state.customers) {
      if (this.state.customers[i].status == "normal") normal += 1;
      else banned += 1;
    }
    for (var i in this.state.sellers) {
      if (this.state.sellers[i].status == "normal") normal += 1;
      else banned += 1;
    }

    this.bannedUserData = {
      labels: ["Normal", "Banned"],
      datasets: [
        {
          data: [normal, banned],
          backgroundColor: ["#3ACD4A", "#CD2323"],
          hoverBackgroundColor: ["#3ACD4A", "#CD2323"]
        }
      ],
      width: 10,
      height: 10
    };

    var tech = 0;
    var cloth = 0;
    var instr = 0;

    var asusNum = 0;
    var appleNum = 0;
    var dellNum = 0;

    var femaleNum = 0;
    var maleNum = 0;
    var kidsNum = 0;
    var babyNum = 0;

    var techStock = 0;
    var clothStock = 0;
    var instStock = 0;

    for (var i in this.state.products) {
      var product = this.state.products[i];
      if (product.category == "technology") {
        techStock += parseInt(product.stock);
        if (product.device == "computer") {
          if (product.brand == "Asus") asusNum += 1;
          else if (product.brand == "Apple") appleNum += 1;
          else if (product.brand == "Dell") dellNum += 1;
        }
        tech += 1;
      } else if (product.category == "clothing") {
        clothStock += parseInt(product.stock);
        if (product.gender == "male") maleNum += 1;
        else if (product.gender == "female") femaleNum += 1;
        else if (product.gender == "boy" || product.gender == "girl")
          kidsNum += 1;
        else if (product.gender == "baby") babyNum += 1;
        cloth += 1;
      } else if (this.state.products[i].category == "instrument") {
        instStock += parseInt(product.stock);
        instr += 1;
      }
    }

    this.categories = {
      labels: ["Technology", "Clothing", "Instrument"],
      datasets: [
        {
          data: [tech, cloth, instr],
          backgroundColor: ["#3B537B", "#7E2E71", "#834F05"],
          hoverBackgroundColor: ["#3B537B", "#7E2E71", "#834F05"]
        }
      ],
      width: 10,
      height: 10
    };

    this.computerCategories = {
      labels: ["Asus", "Apple", "Dell"],
      datasets: [
        {
          data: [asusNum, appleNum, dellNum],
          backgroundColor: ["#7DA4E5", "#8493AC", "#127D94"],
          hoverBackgroundColor: ["#7DA4E5", "#8493AC", "#127D94"]
        }
      ],
      width: 10,
      height: 10
    };

    this.clothingCategories = {
      labels: ["Women", "Men", "Kids", "Babies"],
      datasets: [
        {
          data: [femaleNum, maleNum, kidsNum, babyNum],
          backgroundColor: ["#9942AE", "#452767", "#5DDCBD", "#3BBBDE"],
          hoverBackgroundColor: ["#9942AE", "#452767", "#5DDCBD", "#3BBBDE"]
        }
      ],
      width: 10,
      height: 10
    };

    this.stocks = {
      labels: ["Technology", "Clothing", "Instrument"],
      datasets: [
        {
          label: "Number of Products in the Stock",
          fill: false,
          lineTension: 0.1,
          backgroundColor: "rgba(75,192,192,0.4)",
          borderColor: "rgba(75,192,192,1)",
          borderCapStyle: "butt",
          borderDash: [],
          borderDashOffset: 0.0,
          borderJoinStyle: "miter",
          pointBorderColor: "rgba(75,192,192,1)",
          pointBackgroundColor: "#fff",
          pointBorderWidth: 1,
          pointHoverRadius: 5,
          pointHoverBackgroundColor: "rgba(75,192,192,1)",
          pointHoverBorderColor: "rgba(220,220,220,1)",
          pointHoverBorderWidth: 2,
          pointRadius: 1,
          pointHitRadius: 10,
          data: [techStock, clothStock, instStock]
        }
      ],
      width: 10,
      height: 10
    };
  }

  render() {
    console.log(this.props);
    return (
      <div className="home">
        <NavigationBar
          mode="admin"
          history={this.props.history}
          auth={this.props.location.auth}
          userInfo={this.props.location.userInfo}
          products={this.props.location.products}
        />
        <AdminLeftBar
          sellers={this.state.sellers}
          customers={this.state.customers}
          auth={this.props.location.auth}
          userInfo={this.props.location.userInfo}
          products={this.props.location.products}
        />
        <Container>
          <Row>
            <Col>
              <Card
                className="statistics-title"
                border="dark"
                style={{ width: "15rem" }}
              >
                <Card.Body>
                  <Card.Title>Users</Card.Title>
                </Card.Body>
              </Card>
            </Col>
            <Col>
              <div className="statistics-donut">
                <Doughnut data={this.userData} />{" "}
              </div>
            </Col>
            <Col>
              <div className="statistics-donut">
                <Doughnut data={this.bannedUserData} />{" "}
              </div>
            </Col>
          </Row>
          <Row>
            <Col>
              <Card
                className="statistics-title"
                border="dark"
                style={{ width: "15rem" }}
              >
                <Card.Body>
                  <Card.Title>Products</Card.Title>
                </Card.Body>
              </Card>
            </Col>
            <Col>
              <div className="statistics-donut">
                <Doughnut data={this.categories} />{" "}
              </div>
            </Col>
            <Col>
              <div className="statistics-donut">
                <Doughnut data={this.computerCategories} />{" "}
              </div>
            </Col>
          </Row>
          <Row>
            <Col>
              <div className="statistics-clothing">
                <Doughnut data={this.clothingCategories} />{" "}
              </div>
            </Col>
          </Row>
          <Row>
            <Col>
              <Card
                className="statistics-title"
                border="dark"
                style={{ width: "15rem" }}
              >
                <Card.Body>
                  <Card.Title>Stocks</Card.Title>
                </Card.Body>
              </Card>
            </Col>
            <Col>
              <div className="statistics-stocks">
                <Line data={this.stocks} />{" "}
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}
