import React, { Component } from "react";
import {
  FormGroup,
  FormControl,
  FormLabel,
  Dropdown,
  Button
} from "react-bootstrap";
import "../assets/Sign/Sign.css";
import NavigationBar from "../NavBar/NavigationBar";

export default class Checkout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      address: "",
      cardNo: "",
      cardHolder: "",
      expDate: "",
      securityCode: "",
      isProductBoughtSuccess: "none",
      isCreditCardNoError: "none",
      isSecurityCodeError: "none",
      isExpDateError: "none"
    };
    this.errors = {
      expDateError: "Enter a valid expiration date!",
      creditCardNoError: "Enter a valid credit card number!",
      securityCodeError: "Enter a valid security code!"
    };

    this.successes = {
      productBoughtSuccess: "Product is ordered!"
    };
  }

  validateForm() {
    var isValid =
      this.state.address.length > 0 &&
      this.state.cardNo.length > 0 &&
      this.state.cardHolder.length > 0 &&
      this.state.expDate.length > 0 &&
      this.state.securityCode.length > 0;
    return isValid;
  }

  handleSubmit = e => {
    var isExpDateValid = true;
    try {
      var dateSplit = this.state.expDate.split("/");
      console.log(dateSplit[0], dateSplit[1]);
      var month = parseInt(dateSplit[0]);
      var year = parseInt(dateSplit[1]);
      if (!(month > 0 && month <= 12 && year > 2018)) isExpDateValid = false;
    } catch (err) {
      isExpDateValid = false;
    }
    if (!isExpDateValid) {
      this.setState({
        isSecurityCodeError: "none",
        isProductBoughtSuccess: "none",
        isCreditCardNoError: "none",
        isExpDateError: true
      });
    } else if (this.state.securityCode.length != 3) {
      this.setState({
        isSecurityCodeError: true,
        isProductBoughtSuccess: "none",
        isCreditCardNoError: "none",
        isExpDateError: "none"
      });
    } else if (isNaN(this.state.cardNo) || this.state.cardNo.length != 16) {
      this.setState({
        isSecurityCodeError: "none",
        isProductBoughtSuccess: "none",
        isCreditCardNoError: true,
        isExpDateError: "none"
      });
    } else {
      var userInfo = this.props.location.userInfo;
      const fetch_url = "/api/customer/buy/" + userInfo.userId;
      fetch(fetch_url, {
        method: "GET",
        headers: {
          "session-id": userInfo.sessionId,
          "Content-Type": "application/json"
        }
      });
      this.setState({
        isSecurityCodeError: "none",
        isProductBoughtSuccess: true,
        isCreditCardNoError: "none",
        isExpDateError: "none"
      });

      const { history } = this.props;
      var toHome = {
        pathname: "/home",
        userInfo: userInfo,
        auth: this.props.location.auth,
        products: this.props.location.products
      };
      setTimeout(function() {
        history.push(toHome);
      }, 2000);
    }
  };

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  };

  render() {
    // Handle visibility of NavigationBar components
    return (
      <div>
        <NavigationBar
          mode="customer"
          auth={this.props.location.auth}
          userInfo={this.props.location.userInfo}
          history={this.props.history}
          products={this.props.location.products}
        />{" "}
        <div className="Register">
          <form>
            <FormGroup controlId="address" bsSize="large">
              <FormLabel className="Label">Address</FormLabel>
              <FormControl
                as="textarea"
                className="Input"
                value={this.state.address}
                onChange={this.handleChange}
                type="name"
              />
            </FormGroup>
            <FormGroup controlId="cardNo" bsSize="large">
              <FormLabel className="Label">Credit Card Number</FormLabel>
              <FormControl
                className="Input"
                autoFocus
                type="text"
                value={this.state.cardNo}
                onChange={this.handleChange}
              />
            </FormGroup>
            <div
              style={{
                display: this.state.isCreditCardNoError,
                color: "red",
                marginTop: "-8px"
              }}
            >
              {this.errors.creditCardNoError}
            </div>
            <FormGroup controlId="cardHolder" bsSize="large">
              <FormLabel className="Label">Card Holder</FormLabel>
              <FormControl
                className="Input"
                autoFocus
                value={this.state.cardHolder}
                type="name"
                onChange={this.handleChange}
              />
            </FormGroup>
            <FormGroup controlId="expDate" bsSize="large">
              <FormLabel className="Label"> Expiration Date</FormLabel>
              <FormControl
                className="Input"
                autoFocus
                placeholder="Month/Year"
                value={this.state.expDate}
                onChange={this.handleChange}
                type="name"
              />
            </FormGroup>
            <div
              style={{
                display: this.state.isExpDateError,
                color: "red",
                marginTop: "-8px"
              }}
            >
              {this.errors.expDateError}
            </div>
            <FormGroup controlId="securityCode" bsSize="large">
              <FormLabel className="Label"> Security Code</FormLabel>
              <FormControl
                className="Input"
                autoFocus
                value={this.state.securityCode}
                onChange={this.handleChange}
                type="name"
              />
            </FormGroup>
            <div
              style={{
                display: this.state.isSecurityCodeError,
                color: "red",
                marginTop: "-8px",
                marginBottom: "5px"
              }}
            >
              {this.errors.securityCodeError}
            </div>
            <Button
              onClick={this.handleSubmit}
              variant="dark"
              disabled={!this.validateForm()}
            >
              Order
            </Button>
            <div
              style={{
                display: this.state.isProductBoughtSuccess,
                color: "green",
                marginTop: "10px"
              }}
            >
              <b>{this.successes.productBoughtSuccess}</b>
            </div>
          </form>
        </div>
      </div>
    );
  }
}
